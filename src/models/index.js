'use strict';
import Realm from '../utils/realm';

import Article, { SCHEMA as ARTICLE_SCHEMA } from './article';


const DEFAULT_SCHEMA = [{
    name: 'StringArray',
    properties: {
        value: 'string'
    }
}];

class Model {
    static _instance;

    static get Instance() {
        if (!Model._instance) {
            Model._instance = new this();
        }
        return Model._instance;
    }

    constructor() {
        this.realm = new Realm();
        this.article = new Article(this.realm);
    }

    init() {
        this.realm.connect({ ARTICLE_SCHEMA, DEFAULT_SCHEMA });
        // console.log(this.realm);
    }
}

let ArticleModel = Model.Instance.article;

export default Model.Instance;
export { ArticleModel as Article };