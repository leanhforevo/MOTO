'use strict';

import { Dimensions, Platform } from 'react-native';

const configs = {
    VERSION: "1.0.0",
    MESSAGE_VERSION: 3,
    DEFAULT_WIDTH: 750,
    DEFAULT_HEIGHT: 1336,
    STORAGE_KEY: "_MPAY_",

    MAIL_TO: "",

    ELEVATION:2,
    ANDROID_ID: "",
    APP_ID: "",
    COLOR_PRIMARY: '#d42032',
    COLOR_ITEM: '#d42032',
    COLOR_BTN:'#f8bec4',
    COLOR_BTNDISABLE:'#dc182d',
    FONT: {
        EXTRALIGHT: "Montserrat-ExtraLight",
        LIGHT: "Montserrat-Light",
        THIN: "Montserrat-Thin",
        REGULAR: "Montserrat-Regular",
        MEDIUM: "Montserrat-Medium",
        SEMIBOLD: "Montserrat-SemiBold",
        BOLD: "Montserrat-Bold",
        EXTRABOLD: "Montserrat-ExtraBold",
        BLACK: "Montserrat-Black",
    },

    FONT_DEFAULT: "Montserrat-Light",
    API: 'http://ec2-18-220-97-215.us-east-2.compute.amazonaws.com',
    keyAPI: '2996e0c85918ffa2ecac2336a34c52e6'
};

export default configs;
