
'use strict';

import React, { Component } from 'react';
import { View, Platform, Dimensions, ActivityIndicator, Text } from 'react-native';
const StatusBarManager = require('NativeModules').StatusBarManager;
import Configs from '../../configs';

export default class Statusbar extends Component {
    constructor(props) {
        super(props);
        let height = 0;
        if (Platform.OS == 'ios') {
            if (Dimensions.get('window').width >= 768) {
                height = 40;
            }
            else {
                height = 20;
            }
        }
        else if (Platform.Version >= 21) {
            height = StatusBarManager.HEIGHT;
        }
        this.height = height;
    }

    render() {
        if (this.height) {
            return (<View style={{ height: this.height }} />);
        }
        else {
            return null;
        }
    }
}
export class HR extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={{ width: Dimensions.get('window').width, height: 1, backgroundColor: '#fff' }} />
        );
    }
}
export class Time extends Component {
    constructor(props) {
        super(props)
    }
    formatDate(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return date.getDate() + "/" +(date.getMonth() + 1) + "/" + date.getFullYear() + "  " + strTime;
    }
    render() {
        let data = new Date(this.props.time)
        let e = this.formatDate(data)
        return (
            this.props.time ?
                <Text style={{ color: Configs.COLOR_PRIMARY }}>{e}</Text>
                :
                <Text style={{ color: Configs.COLOR_PRIMARY }}>...</Text>
        );
    }
}
export class Loading extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }} >
                {this.props.data ?
                    <Text>{this.props.data}</Text>
                    :
                    <ActivityIndicator size={'large'} />}
            </View>
        );
    }
}
