'use strict';

import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Dimensions, StyleSheet } from 'react-native';
import Configs from '../../configs';
import { Actions } from 'react-native-router-flux';
const { width, height } = Dimensions.get('window');
export default class PopupInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            km: ''
        }
    }

    render() {
        return (
            <TouchableOpacity
                style={[styles.apso, {
                    backgroundColor: 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center'
                }]}
                onPress={() => { Actions.pop() }}
            >
                {this.renderBlackScreen()}
                {this.renderFormInput()}
            </TouchableOpacity>
        );
    }
    renderBlackScreen() {
        return <View style={[styles.apso, { backgroundColor: 'black', opacity: .5 }]} />
    }
    renderFormInput() {
        return (
            <View style={styles.containForm}>
                <Text style={styles.txtHeader}> Cập nhật Km hiện tại để hệ thống giúp bạn chẩn đoán chính xác hơn</Text>
                <TextInput
                    placeholder={'Nhập km mới'}
                    style={styles.inputtxt}
                    keyboardType={'numeric'}
                    underlineColorAndroid={'transparent'}
                    value={this.state.km}
                    onChangeText={(val) => { this.setState({ km: val }) }}
                    maxLength={6}

                />
                <View style={styles.formBTN}>
                    <View style={styles.containBTN}>
                        <TouchableOpacity style={styles.btn} onPress={()=>this.props.action(this.state.km)}>
                            <Text style={{ color: '#fff' }}>0K</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containBTN}>
                        <TouchableOpacity style={styles.btn} onPress={() => { Actions.pop() }}>
                            <Text style={{ color: '#fff' }}>Hủy</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF'
    },
    apso: {
        position: 'absolute',
        top: 0, left: 0, right: 0, bottom: 0,
    },
    containForm: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: Configs.COLOR_ITEM,
        borderRadius: 4,
        marginLeft: 20,
        marginRight: 20,
        padding: 20
    },
    txtHeader: {
        color: Configs.COLOR_ITEM,
        marginTop: 10,
        textAlign: 'center',
        fontSize: 18
    },
    inputtxt: {
        color: Configs.COLOR_ITEM,
        borderWidth: 1,
        marginTop: 10,
        borderColor: Configs.COLOR_ITEM,
        fontSize: 18,
        textAlign: 'center',
    },
    formBTN: {
        flexDirection: 'row',
        marginTop: 10
    },
    containBTN: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        backgroundColor: Configs.COLOR_ITEM,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        width: width / 3
    }
})