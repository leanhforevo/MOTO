'use strict';

import React, { Component, PropTypes } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Configs from '../../configs';

const { width, height } = Dimensions.get('window');
export class Header extends Component {
    static propTypes = {
        btnBack: PropTypes.bool,
        title: PropTypes.string
    }

    static defaultProps = {
        btnBack: true,
        title: 'Title'
    }
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', backgroundColor: '#d42032', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.pop() }}>
                    <Icon name='reply' size={30} color={'#fff'} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: '#fff', fontWeight: 'bold' }}>{this.props.title}</Text>
                </View>
                {this.props.right ?
                    <TouchableOpacity onPress={this.props.right} style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                        <Icon name='floppy-o' size={35} color={'#fff'} />
                    </TouchableOpacity>
                    :
                    <View style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} />
                }


            </View>
        );
    }
}
export class HeaderTitle extends Component {
    static propTypes = {
        btnBack: PropTypes.bool,
        title: PropTypes.string
    }

    static defaultProps = {
        btnBack: true,
        title: 'Title'
    }
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', backgroundColor: '#d42032', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: '#fff', fontWeight: 'bold' }}>{this.props.title}</Text>
                </View>
            </View>
        );
    }
}
export class HeaderMenu extends Component {
    static propTypes = {
        btnBack: PropTypes.bool,
        title: PropTypes.string
    }

    static defaultProps = {
        btnBack: true,
        title: 'Title'
    }
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', backgroundColor: '#d42032', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.refresh({ key: 'drawer', open: true }) }}>
                    <Icon name='bars' size={30} color={'#fff'} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: '#fff', fontWeight: 'bold' }}>{this.props.title}</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    {/* <Icon name='cog' size={35} color={'#fff'} /> */}
                </TouchableOpacity>

            </View>
        );
    }
}
