'use strict';

import React, {
    Component
} from 'react';

import {
    View,
    Text,
    Image,
    FlatList,
    TouchableOpacity
} from 'react-native';

import Drawer from 'react-native-drawer';
import StatusbarHeight from '../statusbarHeight';
import styles from './style';
import strings from './strings';
import { TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold, TextW } from '../text';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions, DefaultRenderer } from 'react-native-router-flux';
const LOGO = require('../../resources/logo.png');
const LOGO_BOTTOM = require('../../resources/logoBottom.png');
const ARR_ITEMS = [
    { id: 1, icon: 'heart', txt: 'Tình trạng hiện tại xe', action: () => func(Actions.tabInfor()), notiCount: null },//ok
    { id: 2, icon: 'clock-o', txt: 'Lịch sử bảo trì', action: () => func(Actions.maintenanceHistory()), notiCount: null },//ok
    // { id: 3, icon: 'bookmark-o', txt: 'Book bảo trì/Sửa chữa', action: () => func(Actions.maintenanceBook()), notiCount: null },//ok
    { id: 4, icon: 'calendar', txt: 'Lịch bảo trì tiếp theo', action: () => func(Actions.maintenance()), notiCount: null },//ok
    // { id: 5, icon: 'sticky-note', txt: 'Cặp nhật km hiện tại', action: () => func(Actions.popUpInput()), notiCount: 16 },//ok
    //  { id: 6, icon: 'wrench', txt: 'Tìm trạm bảo trì', action: () => func(alert('Page Dose Not Exist')), notiCount: null },
    { id: 7, icon: 'paint-brush', txt: 'Tìm trạm rửa xe', action: () => func(Actions.carDetailClean()), notiCount: 51 },//ok
    { id: 8, icon: 'fire', txt: 'Tìm trạm xăng', action: () => func(Actions.carDetailGas()), notiCount: null },//ok
    //  { id: 9, icon: 'bell', txt: 'Thông báo từ Head', action: () => func(alert('Page Dose Not Exist')), notiCount: null },
    //  { id: 10, icon: 'check-square-o', txt: 'Kiểm tra thời gian bảo hành xe', action: () => func(Actions.maintenanceList()), notiCount: null },//ok
    // { id: 11, icon: 'thumbs-o-up', txt: 'Ý kiến phản hồi khách hàng', action: () => func(Actions.feedBack()), notiCount: null },//ok
    { id: 12, icon: 'gift', txt: 'Khuyển mãi/ Quảng cáo', action: () => func(Actions.tabGif()), notiCount: null },//ok
    // { id: 13, icon: 'share-alt', txt: 'Giới thiệu bạn bè dùng app', action: () => func(Actions.tabShare()), notiCount: null },//ok
    // { id: 14, icon: 'question', txt: 'Hỏi đáp với Head', action: () => func(alert('Page Dose Not Exist')), notiCount: null },
    { id: 15, icon: 'envelope-o', txt: 'Quản lý tin nhắn', action: () => func(Actions.tabMessage()), notiCount: null },//ok
    { id: 16, icon: 'user', txt: 'Thông tin tài khoản', action: () => func(Actions.accountInfo()), notiCount: null },//ok
]
let func = (e) => {
    e
    setTimeout(() => { Actions.refresh({ key: 'drawer', open: false }) })

}
export default class Layout extends Component {
    render() {
        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref="navigation"
                open={state.open}
                onOpen={() => Actions.refresh({ key: state.key, open: true })}
                onClose={() => Actions.refresh({ key: state.key, open: false })}
                type="displace"
                content={this.renderContent()}
                // tapToClose={true}
                openDrawerOffset={0.2}
                panCloseMask={0.05}
                panOpenMask={0.05}
                captureGestures={true}
                tapToClose={true}
                initializeOpen={false}
                negotiatePan={true}
                styles={drawerStyles}
                // tweenHandler={(ratio) => {
                //   return {
                //     mainOverlay: { opacity: ratio === 0 ? 0 : 0.3, backgroundColor: '#000' }
                // }
                //}}
                tweenDuration={150}
                tweenHandler={Drawer.tweenPresets.parallax}
            >
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
            </Drawer>
        );
    }
    renderContent() {
        return (
            <View style={{ flex: 1, backgroundColor: '#eef3fa', borderRightWidth: 1, borderRightColor: 'grey' }}>
                <StatusbarHeight />
                <View style={{ borderBottomColor: 'orange', flexDirection: 'row', borderBottomWidth: 1, justifyContent: 'center' }}>
                    <View style={{ flex: 1, backgroundColor: '#d42032', justifyContent: 'center', alignItems: 'center' }}>
                        <Icon name={'motorcycle'} color={'#fff'} size={50} />
                    </View>
                    <View style={{ flex: 2, backgroundColor: '#d42032', justifyContent: 'center', padding: 10 }}>
                        <TextViewBold style={{ fontWeight: 'bold', color: '#fff' }}>{this.state.account.name}</TextViewBold>
                        <TextViewBold style={{ fontWeight: 'bold', color: '#fff' }}>{this.state.account.email}</TextViewBold>
                        <TextView style={{ color: '#fff' }}>{this.state.account.tel}</TextView>
                    </View>
                </View>
                <View style={{ flex: 1, backgroundColor: '#fff', paddingLeft: 0, paddingTop: 10 }}>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={ARR_ITEMS}
                            keyExtractor={(item, index) => index}
                            renderItem={(value, index) => this.renderItemContent(value.item)}
                        />
                        {/* {this.renderItemContent()} */}
                    </View>

                    <View style={{ padding: 0, justifyContent: 'center', alignItems: 'center', borderTopWidth: 1, borderTopColor: '#d7d7d7' }}>
                        <Image source={LOGO_BOTTOM} resizeMode={'contain'} style={{ width: 100, height: 50 }} />
                    </View>
                </View>

            </View>
        );
    }
    renderItemContent(item) {
        return (
            <TouchableOpacity activeOpacity={.8} onPress={item.action} style={{ flexDirection: 'row', padding: 10, borderBottomWidth: 1, borderBottomColor: '#d7d7d7' }}>
                <Icon name={item.icon} color={'red'} size={35} style={{ width: 40, height: 40 }} />
                <View style={{ flex: 1, backgroundColor: '#fff', flexDirection: 'row', marginLeft: 5 }}>
                    <View style={{ flex: 3, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'flex-start' }}>
                        <TextView style={{ fontSize: 16, color: '#726969' }}>{item.txt}</TextView>
                    </View>
                    {item.notiCount ?
                        <View style={{ flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' }}>
                            <View style={{ padding: 5, backgroundColor: 'red', borderRadius: 100 }}><TextW style={{ color: '#fff' }}>{item.notiCount}</TextW></View>
                        </View>
                        :
                        <View />
                    }

                </View>

            </TouchableOpacity>
        );
    }
}


const drawerStyles = {
    drawer: {
        shadowColor: '#000', shadowOpacity: 0.3, shadowRadius: 1
    },
    main: { paddingLeft: 0 }
}