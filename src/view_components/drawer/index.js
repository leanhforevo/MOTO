'use strict';

import React, { Component } from 'react';
import Layout from './layout';
import Icon from 'react-native-vector-icons/FontAwesome';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../states/actions';
class Drawer extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            account: this.props.app.accountInfor
        }
    //    alert(JSON.stringify(this.props.app.accountInfor))
    }

}
const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return { actions: acts };
};

export default connect(stateToProps, dispatchToProps)(Drawer);
// export default Drawer;