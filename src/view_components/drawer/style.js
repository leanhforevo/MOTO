import {
    StyleSheet,
    Dimensions
} from 'react-native';

const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        paddingTop: 4,
        alignItems: "center",
        justifyContent: 'center',
        width: width / 4,
        height: 50
    },
    tabName: {
        marginTop: 4,
        fontSize: 11,
        color: "#CCCCCC"
    },
    tabNameSelected: {
        marginTop: 4,
        fontSize: 11,
        color: "#0E3563"
    }
});

export default styles;