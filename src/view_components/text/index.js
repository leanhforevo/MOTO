'use strict';

import React, { Component } from 'react';
import { Text } from 'react-native';
import Configs from '../../configs';
const colorTXT = '#5c5c5d'
// colorTXT=colorTXT;/
export class TextView extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={[{ color: colorTXT }, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}
export class TextViewBold extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={[{ fontWeight: 'bold', color: colorTXT }, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}
export class TextViewPrimary extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={[this.props.style, { fontSize: 18, color: colorTXT }]}>
                {this.props.children}
            </Text>
        );
    }
}
export class TextViewPrimaryBold extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={[this.props.style, { fontWeight: 'bold', fontSize: 18, color: colorTXT }]}>
                {this.props.children}
            </Text>
        );
    }
}
export class TextW extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Text style={[{ fontSize: 16, color: '#dc182d' }, this.props.style]}>
                {this.props.children}
            </Text>
        );
    }
}