import {
    StyleSheet,
    Dimensions
} from 'react-native';

const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        paddingTop: 4,
        alignItems: "center",
        justifyContent: 'center',
        width: width / 4,
        height: 50
    },
    tabName: {
        marginTop: 4,
        fontSize: 11,
        color: "#f8bec4"
    },
    tabNameSelected: {
        marginTop: 4,
        fontSize: 11,
        color: "#dc182d"
    }
});

export default styles;