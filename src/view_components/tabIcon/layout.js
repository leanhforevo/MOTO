'use strict';

import React, {
    Component
} from 'react';

import {
    View,
    Text,
    Image,
} from 'react-native';

import styles from './style';
import strings from './strings';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Layout extends Component {
    render() {
        return (
            <View style={styles.container}>
                  {this.icons[this.props.name][this.props.selected ? 1 : 0]}
                <Text style={this.props.selected ? styles.tabNameSelected : styles.tabName}>
                    {strings[this.props.name]}
                </Text>
            </View>
        );
    }
}