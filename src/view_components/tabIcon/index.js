'use strict';

import React, { Component } from 'react';
import Layout from './layout';
import Icon from 'react-native-vector-icons/FontAwesome';

const ICON_INFOR = (<Icon name={'id-card-o'} size={25} color={'#f8bec4'} />);
const ICON_INFOR_SE = (<Icon name={'id-card-o'} size={25} color={'#dc182d'} />);

const ICON_MESSAGE = (<Icon name={'envelope-o'} size={25} color={'#f8bec4'} />);
const ICON_MESSAGE_SE = <Icon name={'envelope-o'} size={25} color={'#dc182d'} />;

const ICON_GIF = (<Icon name={'gift'} size={25} color={'#f8bec4'} />);
const ICON_GIF_SE = (<Icon name={'gift'} size={25} color={'#dc182d'} />);

const ICON_SHARE = (<Icon name={'share-alt'} size={25} color={'#f8bec4'} />);
const ICON_SHARE_SE = (<Icon name={'share-alt'} size={25} color={'#dc182d'} />);



class TabIcon extends Layout {
    constructor(props) {
        super(props);
        this.icons = {
            "tabInfor": [
                ICON_INFOR, ICON_INFOR_SE, 26, 26
            ],
            "tabMessage": [
                ICON_MESSAGE, ICON_MESSAGE_SE, 24, 24
            ],
            "tabGif": [
                ICON_GIF, ICON_GIF_SE, 29, 22
            ],
            "tabShare": [
                ICON_SHARE, ICON_SHARE_SE, 26, 25
            ]
        }
    }
}

export default TabIcon;