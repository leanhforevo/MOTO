'use strict';

import React, { Component } from 'react';
import { Platform, StatusBar } from 'react-native';
import { Provider, connect } from 'react-redux';
import { Router } from 'react-native-router-flux';
// import Reactotron, { trackGlobalErrors, asyncStorage } from 'reactotron-react-native'
import Store from './states/store';
import Storage from './utils/storage';
import Configs from './configs';
import Views from './views';

const RouterWithRedux = connect()(Router);

export default class extends Component {
    constructor() {
        super();
        Storage.STORAGE_KEY = Configs.STORAGE_KEY;
        if (Platform.OS == 'android') {
            StatusBar.setTranslucent(true);
            StatusBar.setBackgroundColor("#d42032");
            StatusBar.setBarStyle('dark-content',true);
        }
        else {
            StatusBar.setBarStyle('dark-content');
        }
        // Reactotron
        //     .configure()
        //     .use(trackGlobalErrors({
        //         veto: frame => frame.fileName.indexOf('/node_modules/react-native/') >= 0
        //     }))
        //     .use(asyncStorage())
        //     .connect();
    }

    render() {
        return (
            <Provider store={Store}>
                <RouterWithRedux scenes={Views} />
            </Provider>
        );
    }
}
