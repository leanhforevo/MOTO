'use strict';

import key from 'keymirror';

const typeSuffix = key({
    SUCCESS: null,
    FAIL: null
});

export const remoteMiddleware = store => next => action => {

    let remote = (url, method, body, callback) => {
        let request = {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                // 'c' : 'd'
            },
        };
        // if (body.token) {
        //     request.headers['Authorization'] = "Bearer " + body.token;
        // }

        if (method == 'POST') {
            request["method"] = method;
            request["body"] = JSON.stringify(body);
        }
        console.log('------------------------------------');
        console.log(url);
        console.log('------------------------------------');
        return fetch(url, request).then(response => response.json()).then(value => resolved({ store, next, action, value, callback }));
    };

    if (action.payload && action.payload.remote && action.payload.api) {
        let url = action.payload.remote + action.payload.api;
        if (action.payload.db) {
            let data = action.payload.db.get();
            let forceUpdate = data && action.payload.db.timeout && action.payload.db.timeout(data);
            if (!data || Object.keys(data).length == 0 || data.length == 0 || forceUpdate) {
                let state = store.getState();
                if (action.payload.token && state.user.token && state.user.username) {
                    action.payload.body.token = state.user.token;
                    action.payload.body.username = state.user.username;
                }

                return remote(url, action.payload.method, action.payload.body, (value) => {
                    if (action.payload.db.set) {
                        action.payload.db.set(value);
                    }
                }).catch(error => {
                    // console.log(error);
                    resolved({ store, next, action, value: { data, error: null } });
                });
            } else {
                // console.log("DB LOADED");
                return new Promise((resolve, reject) => resolve(resolved({ store, next, action, value: { data, error: null } })));
            }
        } else {
            let state = store.getState();
            if (action.payload.token && state.user.token && state.user.username) {
                action.payload.body.token = state.user.token;
                action.payload.body.username = state.user.username;
            }
            // console.log("REMOTE", url, action.payload.method, action.payload.body);
            return remote(url, action.payload.method, action.payload.body).catch(error => {
                console.log(error);
                rejected({ store, next, action, value: { data, error: null } });
            });
        }
    }
    return next(action);
};

let resolved = (res) => {
    console.log(res);
    if (!res.value || res.value.error) {
        return res.next({
            type: res.action.type + "_" + typeSuffix.FAIL,
            payload: {
                error: null
            }
        });
    }
    if (res.callback) {
        res.callback(res.value.data);
    }
    return res.next({
        type: res.action.type + "_" + typeSuffix.SUCCESS,
        payload: res.value,
        params: res.action.params
    });
}

let rejected = (res) => {
    // console.log("FAIL");
    // console.log(res);
    return res.next({
        type: res.action.type + "_" + typeSuffix.FAIL,
        error: res.error
    });
}