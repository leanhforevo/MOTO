'use strict';

export const funcMiddleware = store => next => action => {
    if (action.payload && action.payload.func) {
        return next({
            type: action.type,
            payload: action.payload.func(),
            params: action.params
        });
    }
    return next(action);
};

export const passMiddleware = store => next => action => {
    if (action.payload && action.payload.nextAction){
        return next({
            type: action.type,
            payload : action.payload
        });
    }
    return next(action);
};