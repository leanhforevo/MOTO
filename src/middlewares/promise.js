'use strict';

import key from 'keymirror';

const typeSuffix = key({
    SUCCESS: null,
    FAIL: null
});

export const promiseMiddleware = store => next => action => {
    if (action.payload && action.payload.promise) {
        return action.payload.promise
            .then((value) => resolved({ store, next, action, value }));
        // .catch((error) => rejected({ store, next, action, error }));
    }
    return next(action);
};

function resolved(res) {
    if (!res.value) {
        return res.next({
            type: res.action.type + "_" + typeSuffix.FAIL,
            payload: {
                error: "null"
            }
        });
    }
    return res.next({
        type: res.action.type + "_" + typeSuffix.SUCCESS,
        payload: res.value,
        params: res.action.params
    });
}

function rejected(res) {
    return res.next({
        type: res.action.type + "_" + typeSuffix.FAIL,
        error: res.error
    });
}