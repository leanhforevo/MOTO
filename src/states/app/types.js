'use strict';

import key from 'keymirror';

export default key({
    SET_UPDATE_APP: null,

    GET_PUSHNOTI: null,
    GET_PUSHNOTI_SUCCESS: null,
    GET_PUSHNOTI_FAIL: null,

    SET_PUSHNOTI: null,
    SET_PUSHNOTI_SUCCESS: null,
    SET_PUSHNOTI_FAIL: null,

    GET_DEVICE_INFO: null,

    //get account
    GET_REGIS: null,
    GET_REGIS_SUCCESS: null,
    GET_REGIS_FAIL: null,

    GET_ACCINFOR: null,
    GET_ACCINFOR_SUCCESS: null,
    GET_ACCINFOR_FAIL: null,

    SET_ACCINFOR: null,
    SET_ACCINFOR_SUCCESS: null,
    SET_ACCINFOR_FAIL: null,

    GET_BIKE: null,
    GET_BIKE_SUCCESS: null,
    GET_BIKE_FAIL: null,

    SET_BIKE: null,
    SET_BIKE_SUCCESS: null,
    SET_BIKE_FAIL: null,

    SET_BIKESELECTED: null,
    SET_BIKESELECTED_SUCCESS: null,
    SET_BIKESELECTED_FAIL: null,

    GET_MAINTAIN_NOW: null,
    GET_MAINTAIN_NOW_SUCCESS: null,
    GET_MAINTAIN_NOW_FAIL: null,

    GET_MAINTAIN_HIS: null,
    GET_MAINTAIN_HIS_SUCCESS: null,
    GET_MAINTAIN_HIS_FAIL: null,

}, "APP");
