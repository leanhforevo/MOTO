'use strict';

import { Record } from 'immutable';

const InitialState = Record({
    updateProgress: null,
    pushNoti: false,
    deviceInfo: null,
    regis: {},
    accountInfor: {},
    listBike: {},
    bikeSelected:{},
    maintainNow:{},
    maintainHis:{}

});

export default InitialState;
