'use strict';

import InitialState from './';
import types from './types';
import createReducer from '../reducer';

import DeviceInfo from 'react-native-device-info';

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.SET_UPDATE_APP]: (state, data) => state.set('updateProgress', Object.assign({}, data)),

    [types.GET_PUSHNOTI_SUCCESS]: (state, data) => state.set('pushNoti', isNaN(data) || data == 0 ? false : true),
    [types.SET_PUSHNOTI_SUCCESS]: (state, data) => state.set('pushNoti', isNaN(data) || data == 0 ? false : true),

    [types.GET_DEVICE_INFO]: (state, data) => {
        let deviceInfo = {
            uniqueID: DeviceInfo.getUniqueID(),
            manufacturer: DeviceInfo.getManufacturer(),
            brand: DeviceInfo.getBrand(),
            model: DeviceInfo.getModel(),
            deviceId: DeviceInfo.getDeviceId(),
            systemName: DeviceInfo.getSystemName(),
            systemVersion: DeviceInfo.getSystemVersion(),
            bundleId: DeviceInfo.getBundleId(),
            buildNumber: DeviceInfo.getBuildNumber(),
            version: DeviceInfo.getVersion(),
            readableVersion: DeviceInfo.getReadableVersion(),
            deviceName: DeviceInfo.getDeviceName(),
            userAgent: DeviceInfo.getUserAgent(),
            deviceLocale: DeviceInfo.getDeviceLocale(),
            deviceCountry: DeviceInfo.getDeviceCountry(),
            timezone: DeviceInfo.getTimezone(),
            instanceID: DeviceInfo.getInstanceID(),
            isEmulator: DeviceInfo.isEmulator(),
            isTable: DeviceInfo.isTablet()
        };
        return state.set('deviceInfo', Object.assign({}, deviceInfo));
    },

    [types.GET_REGIS_SUCCESS]: (state, data) => state.set('regis', data),

    [types.GET_ACCINFOR_SUCCESS]: (state, data) => state.set('accountInfor', data),
    [types.SET_ACCINFOR_SUCCESS]: (state, data) => state.set('accountInfor', data),
    [types.SET_ACCINFOR_FAIL]: (state, data) => state.set('accountInfor', {}),

    [types.GET_BIKE_SUCCESS]: (state, data) => state.set('listBike', data),
    [types.SET_BIKE_SUCCESS]: (state, data) => state.set('listBike', data),

    [types.GET_MAINTAIN_NOW_SUCCESS]: (state, data) => state.set('maintainNow', data),
    [types.GET_MAINTAIN_NOW_FAIL]: (state, data) => state.set('maintainNow', {}),

    [types.GET_MAINTAIN_HIS_SUCCESS]: (state, data) => state.set('maintainHis', data),
    [types.GET_MAINTAIN_HIS_FAIL]: (state, data) => state.set('maintainHis', {}),

    [types.SET_BIKESELECTED]: (state, data) => state.set('bikeSelected', data),
});