'use strict';

import types from './types';
import Configs from '../../configs';
import store from '../store';
export function updateApp(progress) {
    progress.nextAction = true;
    return next => next({
        type: types.SET_UPDATE_APP,
        payload: progress
    });
}

export function getPushNoti() {
    return next => next({
        type: types.GET_PUSHNOTI,
        payload: {
            storage: {
                key: "PUSHNOTI",
                type: "getInt"
            }
        }
    });
}

export function setPushNoti(status) {
    return next => next({
        type: types.SET_PUSHNOTI,
        payload: {
            storage: {
                key: "PUSHNOTI",
                type: "setInt",
                data: status ? 1 : 0
            }
        }
    });
}

export function getDeviceInfo() {
    return next => next({
        type: types.GET_DEVICE_INFO,
        payload: {
            nextAction: true
        }
    });
}
//register
export function getRegister(name, gender, tel, email) {
    return next => next({
        type: types.GET_REGIS,
        payload: {
            remote: Configs.API,
            api: '/api/user/register?key=' + Configs.keyAPI + '&name=' + name + '&gender=' + gender + '&tel=' + tel + '&email=' + email + '',
            method: 'GET',
        }
    });
}

export function getMaintainNow(user_vehicle_id) {
    return next => next({
        type: types.GET_MAINTAIN_NOW,
        payload: {
            remote: Configs.API,
            api: '/api/maintenance/maintenance-schedule?key=' + Configs.keyAPI + '&user_vehicle_id='+user_vehicle_id,
            method: 'GET',
        }
    });
}

export function getMaintainHis(user_vehicle_id) {
    return next => next({
        type: types.GET_MAINTAIN_HIS,
        payload: {
            remote: Configs.API,
            api: '/api/maintenance/maintenance-history?key=' + Configs.keyAPI + '&user_vehicle_id='+user_vehicle_id,
            method: 'GET',
        }
    });
}

export function getCompetitionsAPI() {
    return {
        type: types.GET_COMPETITION,
        payload: {
            remote: Configs.API,
            api: '/competitions',
            method: 'GET',

        }
    }
}
export function getAccountInfor() {
    return next => next({
        type: types.GET_ACCINFOR,
        payload: {
            storage: {
                key: "ACCOUNTLOCAL1",
                type: "getOnlyObject"
            }
        }
    });
}

export function setAccountInfor(account) {
    return next => next({
        type: types.SET_ACCINFOR,
        payload: {
            storage: {
                key: "ACCOUNTLOCAL1",
                type: "setOnlyObject",
                data: account
            }
        }
    });
}
export function removeAccount() {
    return next => next({
        type: types.SET_ACCINFOR,
        payload: {
            storage: {
                  key: "ACCOUNTLOCAL1",
                type: "setOnlyObject",
                data: null
            }
        }
    });
}
//List Bikek
export function getBike() {
    return next => next({
        type: types.GET_BIKE,
        payload: {
            storage: {
                key: "LISTBIKE",
                type: "getOnlyObject"
            }
        }
    });
}

export function setBike(bike) {
    return next => next({
        type: types.SET_BIKE,
        payload: {
            storage: {
                key: "LISTBIKE",
                type: "setOnlyObject",
                data: bike
            }
        }
    });
}
export function removeBike(bike) {
    return next => next({
        type: types.SET_BIKE,
        payload: {
            storage: {
                key: "LISTBIKE",
                type: "setOnlyObject",
                data: null
            }
        }
    });
}

export function setBikeSelected(bike) {
    return next => next({
        type: types.SET_BIKESELECTED,
        payload: bike
    });
}