'use strict';

import types from './types';

export function setTextHeight(data) {
    return next => next({
        type: types.SET_TEXT_HEIGHT,
        payload: data
    });
}
