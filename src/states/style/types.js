'use strict';

import key from 'keymirror';

export default key({
    SET_TEXT_HEIGHT: null
}, "STYLE");
