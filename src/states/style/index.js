'use strict';

import { Record } from 'immutable';

const InitialState = Record({
    textHeight: ''
});

export default InitialState;
