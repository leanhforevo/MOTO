'use strict';

import InitialState from './';
import types from './types';
import createReducer from '../reducer';

const initialState = new InitialState();

export default createReducer(initialState, {
    [types.SET_TEXT_HEIGHT]: (state, data) => state.set('textHeight', data)
});
