'use strict';

import InitialState from './';
import { Platform } from 'react-native';
import { ActionConst } from 'react-native-router-flux';
// import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge';
import Configs from '../../configs';
import createReducer from '../reducer';

const initialState = new InitialState();

export default createReducer(initialState, {
    [ActionConst.FOCUS]: (state, data, params, action) => {
        // let tracker = new GoogleAnalyticsTracker(Configs.GA_ID[Platform.OS]);
        // tracker.trackScreenView(action.scene.name);
        return state.set('scene', action.scene);
    }
});
