'use strict';

import { Record } from 'immutable';

const InitialState = Record({
    scene: {}
});

export default InitialState;
