'use strict';

import * as app from './app/action';
import * as style from './style/action';

export { app, style };
