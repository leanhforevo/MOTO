'use strict';

import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension'
import reducers from './reducers';
import { storageMiddleware, promiseMiddleware, remoteMiddleware, funcMiddleware, passMiddleware } from '../middlewares';

// const logger = createLogger();
// const finalCreateStore = compose(
const finalCreateStore = composeWithDevTools(
    applyMiddleware(thunk, funcMiddleware, promiseMiddleware, storageMiddleware, remoteMiddleware, passMiddleware)
)(createStore);
const store = finalCreateStore(reducers);

export default store;
