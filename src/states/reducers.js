'use strict';

import { combineReducers } from 'redux';
import router from './router/reducer';
import app from './app/reducer';
import style from './style/reducer';

const rootReducer = combineReducers({
    router,
    app,
    style
});

export default rootReducer;
