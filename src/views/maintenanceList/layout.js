'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import { TextViewPrimaryBold, TextW } from '../../view_components/text';
import { Header } from '../../view_components/header';
import Icon from 'react-native-vector-icons/FontAwesome';
import StarRating from 'react-native-star-rating';
import { Switch } from 'react-native-switch';
const { width, height } = Dimensions.get('window');

export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Lịch bảo trì chi tiết'} />
                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                    {this.renderHeader()}
                    <View style={{ flex: 1, padding: 10, flexDirection: 'row' }}>
                        <View style={{ flex: 1, justifyContent: 'flex-start' }}>
                            <Text numberOfLines={1} style={{ fontSize: 17, color: Configs.COLOR_PRIMARY }}>Các mục thay thế</Text>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'flex-end' }}>

                        </View>
                    </View>
                    {this.renderListItem()}
                </ScrollView>
            </View>
        );
    }
    renderHeader() {
        return (
            <View style={{
                padding: 10, justifyContent: 'center', alignItems: 'center', paddingLeft: 20, paddingRight: 20,
                backgroundColor: '#ededed'
            }}>
                <View style={{ width: 50, height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: Configs.COLOR_PRIMARY, borderRadius: 100 }}>
                    <Icon name={this.state.icon} color={'#fff'} size={35} />
                </View>
                <Text numberOfLines={2} style={{ textAlign: 'center', marginTop: 5 }}>
                    {this.state.title}
                </Text>
            </View>
        );
    }
    renderListItem() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList

                    style={{}}
                    data={this.state.data}
                    keyExtractor={(value, index) => index}
                    renderItem={(value) => this.renderItem(value)}
                />

            </View>
        );
    }
    renderItem(value) {
        value = value.item
        return (
            <View style={{ flex: 1, marginBottom: 10, padding: 10, justifyContent: 'center', alignItems: 'flex-start', backgroundColor: '#ededed' }}>
                <Text>{value.item_maintenance_name}</Text>

            </View>
        )
    }
}
