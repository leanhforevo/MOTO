'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Layout from './layout';

class MaintenanceList extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            title: props.title,
            icon: props.icon
        }
    }

}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(MaintenanceList);
