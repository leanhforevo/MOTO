'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Layout from './layout';

class Maintenance extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false, refreshing: false,
            data: {}
        }
    }
    componentDidMount() {
        this.loadData();
    }
    loadData() {
        this.setState({ loaded: false, refreshing: true })
        this.props.actions.app.getMaintainNow(this.props.app.bikeSelected.user_vehicle_id)
            .then(() => {
                console.log('-----------load dadadada-------------------------');
                console.log(this.props.app.maintainNow);
                console.log('--------------load dadadada----------------------');
                this.setState({ loaded: true, refreshing: false, data: this.props.app.maintainNow })
            });
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(Maintenance);
