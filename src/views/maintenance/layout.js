'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, FlatList, RefreshControl } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight, { Loading } from '../../view_components/statusbarHeight';
import { TextViewPrimaryBold, TextW } from '../../view_components/text';
import { Header } from '../../view_components/header';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                {/* {this.renderHeader()} */}
                <Header title={'Lịch bảo trì tiếp theo'} />
                <View style={{ flex: 1, backgroundColor: '#fff' }}>
                    <View style={{ flexDirection: 'row', padding: 10, marginBottom: 10 }}>
                        <View style={{ flex: 1, backgroundColor: '#ececec', borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                            <TextW style={{}}>2017</TextW>
                        </View>
                        <View style={{ flex: 3, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 10 }}>
                            <Text>Chon tung khung de xem chi tiet</Text>
                        </View>
                    </View>
                    {this.state.loaded && this.state.data ?
                        this.state.data.statusCode == "000" ?
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={() => { this.componentDidMount() }}
                                    />
                                }
                                style={{}}
                                data={this.state.data.items}
                                keyExtractor={(value, index) => index}
                                renderItem={(value) => this.renderItem(value)}
                            />
                            :
                            <Loading data={this.state.data.alert_message} />//not found
                        :
                        <Loading />
                    }
                    {/* {this.renderItem()}
                    {this.renderItem()}
                    {this.renderItem()} */}
                </View>
            </View>
        );
    }
    renderHeader() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.pop() }}>
                    <Icon name='chevron-left' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: Configs.COLOR_ITEM }}>VESPA 2017</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    <Icon name='cog' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>

            </View>
        );
    }
    renderItem(value) {
        value = value.item
        let txt = "";
        value.list_item_maintenance.map((value, index) => {
            txt += value.item_maintenance_name + ", "
        })
        let countStatus = _.groupBy(value.list_item_maintenance, 'maintenance_type');
        return (
            <TouchableOpacity
                style={{ backgroundColor: '#ededed', padding: 10, marginBottom: 10, borderRadius: 2, elevation: Configs.ELEVATION }}
                onPress={() => { Actions.maintenanceDetail({ data: value, actionLoad: () => this.loadData() }) }}
            >
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', borderRightWidth: .5, borderRightColor: '#cccbcb' }}>
                        <Text>{value.maintenance_schedule_name}</Text>
                        <Text>{value.distance_check} Km</Text>
                    </View>
                    <View style={{ flex: 3, padding: 10, justifyContent: 'center', alignItems: 'flex-start' }}>
                        {this.renderTXTBN('Thay thế: ', txt)}
                        {countStatus[0]?this.renderTXTBN('Kiểm tra-Lau chùi: ', countStatus[0].length + ' hạng mục'):null}
                        {/* {countStatus[1]?this.renderTXTBN('Thay thế: ', countStatus[1].length + ' hạng mục'):null} */}
                    </View>
                </View>
                <View style={{ alignItems: 'flex-end' }}>
                    {/* <Text> -/-/----</Text> */}
                </View>
            </TouchableOpacity>
        );
    }
    renderTXTBN(txtHead, txtInfo) {
        return (
            <View style={{ flexDirection: 'row', flexWrap: 'nowrap' }}>
                <Text style={{ fontWeight: 'bold' }}>{txtHead}</Text>
                <Text style={{ flex: 1 }} numberOfLines={2}>{txtInfo}</Text>
            </View>
        );
    }
}
