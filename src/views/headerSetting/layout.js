'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';

const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                {this.renderHeader()}




            </View>
        );
    }

    renderHeader() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.pop() }}>
                    <Icon name='reply' size={30} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: Configs.COLOR_ITEM }}>VESPA 2017</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    <Icon name='cog' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>

            </View>
        );
    }

}
