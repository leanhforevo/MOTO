'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, Geolocation, PermissionsAndroid, Image, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');
import MapView from 'react-native-maps';
const LOCATION_IMAGE = require('../../resources/pin-9-512.gif')
export default class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: {
                latitude: 10.7863321,
                longitude: 106.6731023,
            },
            region: {
                latitude: 10.7863321,
                longitude: 106.6731023,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            },
            arrGas: [
                {
                    position: {
                        latitude: 10.7873321,
                        longitude: 106.6741023,
                    },
                    title: 'gas1',
                    description: 'des1'
                }
            ]
        }
    }
    async componentDidMount() {
        //    let a= await PermissionsAndroid.check('android.permission.ACCESS_FINE_LOCATION');
        //    alert(a);
        navigator.geolocation.getCurrentPosition(
            ({ coords }) => {
                console.log(coords)
                const { latitude, longitude } = coords
                this.setState({
                    position: {
                        latitude,
                        longitude,
                    },
                    region: {
                        latitude,
                        longitude,
                        latitudeDelta: 0.001,
                        longitudeDelta: 0.001,
                    }
                })
            },
            (error) => alert(JSON.stringify(error)),
            // 'enableHighAccuracy' sometimes causes problems
            // If it does, just remove it.
            { enableHighAccuracy: true }
        )
    }
    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                {this.renderHeader()}
                <MapView
                    style={{ flex: 1 }}
                    region={this.state.region}
                    onPress={(val) => { console.log(val) }}
                >
                    <FlatList
                        data={this.state.arrGas}
                        keyExtractor={(val, index) => index}
                        renderItem={(val, index) => this.renderItemMaker(val.item)} />
                    <MapView.Marker.Animated
                        coordinate={this.state.position}
                        title={'marker.title'}
                        description={'marker.description'}
                        anchor={{ x: 0.1, y: 0.2 }}
                    >
                        <Text style={{ color: '#0078bd' }} >Vi trí của bạn</Text>
                        <Icon name='circle' size={18} color='#0078bd' />
                    </MapView.Marker.Animated>
                    {/* <Icon name='rocket' size={18} color='red' /> */}
                </MapView>
            </View>
        );
    }
    renderItemMaker(val) {
        return (
            <MapView.Marker.Animated
                coordinate={val.position}
                title={val.title}
                description={val.description}
                anchor={{ x: 0.1, y: 0.2 }}
            >
                <Icon name='fire' size={18} color='orange' />
            </MapView.Marker.Animated>
        );
    }
    renderHeader() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.pop() }}>
                    <Icon name='chevron-left' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: Configs.COLOR_ITEM }}>VESPA 2017</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    <Icon name='cog' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>

            </View>
        );
    }

}
