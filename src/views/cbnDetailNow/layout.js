'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Animated, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from '../../view_components/rn-viewpager';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';
import PieChart from './pieChart';
import StackedBarChart from './stackedBarChart';
import DropdownAlert from 'react-native-dropdownalert'
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff', padding: 5 }}>
                <ScrollView style={[style.container, {}]} showsVerticalScrollIndicator={false}>
                    {this.renderInfor()}
                    {this.renderNext()}
                    {this.renderCarInfor()}
                    {this.state.chart ?
                        <PieChart data={this.state.chart} />
                        :
                        null
                    }
                    {this.state.chart ?
                        <StackedBarChart data={this.state.chart} />
                        :
                        null
                    }
                </ScrollView>
                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false}
                    translucent={true}
                    style={{ paddingTop: 20, padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }

    renderInfor() {
        return (
            <View style={{ flexDirection: 'column', padding: 5, marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ padding: 10 }}>
                        <Icon style={{ marginLeft: 10, marginRight: 20 }} name={'motorcycle'} color={Configs.COLOR_ITEM} size={30} />
                    </View>
                    <View style={{ flex: 1, padding: 10 }}>
                        <TextViewPrimary style={{ color: '#474849' }}>Xe đã đi: {this.state.data.current_km}Km </TextViewPrimary>

                        <TextViewPrimary style={{ color: '#474849' }}>Bảo trì kế tiếp tại: {this.state.data.next_distance_check} KM</TextViewPrimary>
                        <Text>Cập nhật lúc: {this.state.data.update_time} </Text>
                    </View>
                </View>

                <TouchableOpacity
                    disabled={this.state.btnUpKm}
                    onPress={() => { Actions.popUpInput({ action: (km) => { this._updateKM(km) } }) }}
                    style={{ elevation: Configs.ELEVATION, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', padding: 10, marginBottom: 5, backgroundColor: '#ebebeb' }}>
                    {this.state.btnUpKm ?
                        <ActivityIndicator size={'small'} />
                        :
                        <TextViewPrimary>Cập nhật Km hiện tại </TextViewPrimary>
                    }
                    <Icon name={'pencil-square-o'} size={25} color={'#f37c2a'} />
                </TouchableOpacity>
            </View>
        );
    }
    renderNext() {
        return (
            <View style={{ padding: 5, marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ padding: 10 }}>
                        <Icon style={{ marginLeft: 10, marginRight: 20 }} name={'motorcycle'} color={Configs.COLOR_ITEM} size={30} />
                    </View>
                    <View style={{ flex: 1, padding: 10 }}>
                        <TextViewPrimary style={{ color: '#474849' }}>Lịch bảo trì kế tiếp </TextViewPrimary>

                    </View>
                </View>

                <TouchableOpacity
                    onPress={() => { Actions.maintenance() }}
                    style={{ elevation: Configs.ELEVATION, marginBottom: 5, justifyContent: 'center', alignItems: 'center', padding: 10, backgroundColor: '#ebebeb' }}>
                    <View style={{ flexDirection: 'row', margin: 10 }}>
                        <View style={{ alignItems: 'center', marginRight: 20, borderRadius: 18, backgroundColor: '#d42032', padding: 6, justifyContent: 'center' }}>
                            <Icon name={'calendar-check-o'} color={'#fff'} size={18} />
                        </View>
                        <View style={{ alignItems: 'center', marginRight: 20, borderRadius: 18, backgroundColor: '#d42032', padding: 6, justifyContent: 'center' }}>
                            <Icon name={'refresh'} color={'#fff'} size={18} />
                        </View>
                        <View style={{ alignItems: 'center', marginRight: 20, borderRadius: 18, backgroundColor: '#d42032', padding: 6, justifyContent: 'center' }}>
                            <Icon name={'leaf'} color={'#fff'} size={18} />
                        </View>
                    </View>
                    <TextViewPrimary>Xem Chi Tiết </TextViewPrimary>

                </TouchableOpacity>
            </View>
        );
    }
    renderCarInfor() {
        return (
            <View style={{ flexDirection: 'column', padding: 5, marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ padding: 10 }}>
                        <Icon style={{ marginLeft: 10, marginRight: 20 }} name={'motorcycle'} color={Configs.COLOR_ITEM} size={30} />
                    </View>
                    <View style={{ flex: 1, padding: 10 }}>
                        <TextViewPrimary style={{ color: '#474849' }}>Thông Tin Xe</TextViewPrimary>
                        <TextView>{this.state.data.vehicle_type_name} {this.state.data.year}</TextView>
                        <TextView>Hãng: {this.state.data.motorcycle_manufacturer_name}</TextView>
                        <TextView>Màu: {this.state.data.vehicle_color_name}</TextView>
                        <TextView style={{ color: '#474849' }}>Biển kiểm soát: {this.state.data.license_plate} </TextView>
                        <TextView>Phiên bản: {this.state.data.vehicle_sub_type_name}</TextView>
                    </View>
                </View>
            </View>
        );
    }
}
