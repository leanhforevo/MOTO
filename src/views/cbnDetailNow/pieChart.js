'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, processColor } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import styles from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold, TextW } from '../../view_components/text';
import { PieChart } from 'react-native-charts-wrapper';

const { width, height } = Dimensions.get('window');
export default class PagePieChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            legend: {
                enabled: true,
                textSize: 8,
                form: 'CIRCLE',
                position: 'RIGHT_OF_CHART',
                wordWrapEnabled: true,
            },
            data: {
                dataSets: [{
                    values: [
                        { value: this.props.data.items[0].good != 0 ? this.props.data.items[0].good :  .09, label: this.props.data.items[0].good > 0 ? 'Tình trạng tốt' : '' },
                        { value: this.props.data.items[0].prepare_check != 0 ? this.props.data.items[0].prepare_check : .09, label: this.props.data.items[0].prepare_check > 0 ? 'Chuẩn bị bảo trì' : '' },
                        { value: this.props.data.items[0].bad != 0 ? this.props.data.items[0].bad : .09, label: this.props.data.items[0].bad > 0 ? 'Quá hạn bảo trì' : '' },
                    ],
                    label: '',
                    config: {
                        valueFormatter: '###',
                        colors: [
                            this.props.data.items[0].good > 0 ? processColor('#8CEAFF') : processColor('#FFFFFF'),
                            this.props.data.items[0].prepare_check > 0 ? processColor('#FFD08C') : processColor('#FFFFFF'),
                            this.props.data.items[0].bad > 0 ? processColor('#ff8c9d') : processColor('#FFFFFF')
                        ],
                        valueTextSize: 10,
                        valueTextColor: processColor('#fff'),
                        sliceSpace: 2,
                        selectionShift: 15
                    }
                }],
            }
        };
    }
    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }
    }
    render() {
        return (
            <View style={{ width: width - 10, height: width, marginTop: 10 }}>

                {/* <View style={{ height: 80 }}>
                    <Text> selected entry</Text>
                    <Text> {this.state.selectedEntry}</Text>
                </View> */}

                <View style={[styles.container, { elevation: Configs.ELEVATION, marginBottom: 5, borderWidth: 1, borderColor: '#f0f0f0' }]}>
                    <View style={{
                        marginLeft: 5,
                        marginRight: width / 2,
                        marginTop: 2,
                        padding: 5,
                        backgroundColor: '#f0f0f0',
                        borderRadius: 4,
                        alignItems: 'center', justifyContent: 'center',
                        elevation: Configs.ELEVATION,
                    }}>
                        <TextW> Tổng Quan</TextW>
                    </View>
                    <PieChart
                        style={styles.chart}
                        logEnabled={true}
                        chartBackgroundColor={processColor('#fff')}
                        data={this.state.data}
                        legend={this.state.legend}
                        chartDescription={{
                            text: '',
                            textSize: 15,
                            textColor: processColor('darkgray')
                        }}

                        entryLabelColor={processColor('black')}
                        entryLabelTextSize={10}
                        drawEntryLabels={false}
                        animation={{ durationY: 1500 }}
                        rotationEnabled={false}
                        drawSliceText={false}
                        usePercentValues={false}
                        centerText={'Tổng Quan'}
                        centerTextRadiusPercent={100}
                        holeRadius={30}
                        holeColor={processColor('#f0f0f0')}
                        transparentCircleRadius={45}
                        transparentCircleColor={processColor('#f0f0f088')}
                        maxAngle={360}
                        onSelect={this.handleSelect.bind(this)}

                    />
                </View>

            </View>
        );
    }

}
