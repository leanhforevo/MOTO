'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Layout from './layout';
import DataAPI from '../../utils/fetchData';
import Configs from '../../configs'
import { Actions } from 'react-native-router-flux';
import {
    Platform,
    NetInfo,
    AppState,
    Linking,
    StatusBar,
    BackHandler,
    ToastAndroid, Alert
} from 'react-native';
class CBNNow extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            data: props.app.bikeSelected,
            btnUpKm: false,
            chart: null
        }
        this.backHandlerE = null
    }
    componentWillMount() {
        if (Platform.OS == "android") {
            this.backHandlerE = BackHandler.addEventListener('hardwareBackPress', () => {
                if (this.props.router.scene.name == 'main' || this.props.router.scene.name == 'tabMessage') {
                    let now = new Date().getTime();
                    if (this.lastPress && (now - this.lastPress) <= 2000) {
                        BackHandler.exitApp(0)
                        delete this.lastPress;
                        return true;
                    } else {
                        this.lastPress = now
                        ToastAndroid.show('Nhấn lần nữa để thoát !', ToastAndroid.SHORT);
                        return true;
                    }
                } else if (this.props.router.scene.name == 'drawer') {
                    Actions.refresh({ key: 'drawer', open: false })
                    return true;
                } else {
                    Actions.pop();
                    return true;
                }
            })
        }

        //load dât
        this._loadDataChart();
    }
    componentWillUnmount() {
        if (Platform.OS == "android") {
            this.backHandlerE.remove();
        }
    }
    _updateKM(km) {
        Actions.pop();
        this.setState({ btnUpKm: true })
        let url = Configs.API + '/api/maintenance/check-change-distance?key=' + Configs.keyAPI + '&user_vehicle_id=' + this.state.data.user_vehicle_id + '&distance_current=' + km;
        DataAPI.fetchDataAPI(url, async (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                    this.setState({ btnUpKm: false })
                } else if (data.status == 1) {
                    Alert.alert(
                        'Thông báo.',
                        'Bạn đã tồn tại lịch sử bảo trì, bạn có muốn xóa lịch sử bảo trì không',
                        [
                            { text: 'Có', onPress: () => this._forceUpdateKM(km, 0) },
                            { text: 'Không', onPress: () => this._forceUpdateKM(km, 0) },
                        ],
                        { cancelable: false }
                    )
                    this.setState({ btnUpKm: false })
                } if (data.status == 0) {
                    this._forceUpdateKM(km, 0)
                    this.setState({ btnUpKm: false })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', 'Lỗi')
                this.setState({ btnUpKm: false })
            }
        })
    }
    _forceUpdateKM(km, removeHis) {
        this.setState({ btnUpKm: true })
        let url = Configs.API + '/api/maintenance/regist-distance?key=' + Configs.keyAPI + '&user_vehicle_id=' + this.state.data.user_vehicle_id + '&distance_current=' + km + '&del_history=' + removeHis;
        DataAPI.fetchDataAPI(url, async (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                    this.setState({ btnUpKm: false })
                } else {
                    let item = await this.state.data
                    item.current_km = km;
                    await this.props.actions.app.setBikeSelected(item)
                    await this.dropdown.alertWithType('success', 'SUCCESS', 'Cập nhật thành công')
                    this.setState({ btnUpKm: false })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', 'Lỗi')
                this.setState({ btnUpKm: false })
            }
        })
    }
    _loadDataChart() {
        let url = Configs.API + '/api/maintenance/health-chart?key=' + Configs.keyAPI + '&user_vehicle_id=' + this.state.data.user_vehicle_id;
        DataAPI.fetchDataAPI(url, (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi! không lấy được thông tin chart')
                } else {
                    console.log(data)
                    this.setState({ chart: data })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi! không lấy được thông tin chart')
            }
        })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(CBNNow);
