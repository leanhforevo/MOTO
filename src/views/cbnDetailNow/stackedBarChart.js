'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, processColor } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import styles from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold, TextW } from '../../view_components/text';
import { BarChart } from 'react-native-charts-wrapper';
import _ from 'lodash'
const { width, height } = Dimensions.get('window');
export default class StackedBarChart extends Component {
    constructor(props) {
        super(props);
        this.colorsPro = []
        this.chartY = []

        props.data.detail.map((val, index) => {
            var num = 0;
            if (val.percent >= 100 || val.percent < 0) {
                this.chartY.push(
                    {
                        y: [100, .09],
                        marker: ['Đã sử dụng', 'Tốt']
                    })
                this.colorsPro.push(processColor('#726969'))
            } else {
                this.colorsPro.push(processColor('#726969'), processColor('red'))
                this.chartY.push(
                    {
                        y: [val.percent, (100 - val.percent)],
                        marker: ['Đã sử dụng', 'Tốt']
                    })
            }

        })

        this.chartX = []
        props.data.detail.map((val, index) => {
            try {
                if (val.item_maintenance_name == "Các mục kiểm tra và lau chùi") {
                    this.chartX.push('K.tra/L.Chùi')
                } else if (val.item_maintenance_name == "Bánh xe, lốp xe") {
                    this.chartX.push('Bánh, lốp xe')
                } else if (val.item_maintenance_id == "19") {
                    this.chartX.push('Dầu truyền')
                } else if (val.item_maintenance_name == "Dung dịch làm mát") {
                    this.chartX.push('D.D làm mát')
                } else {
                    this.chartX.push(val.item_maintenance_name)
                }
            } catch (error) {
                this.chartX.push('...')
                // 8this.chartX.push(val.item_maintenance_name)
            }

        })
        this.state = {
            legend: {
                enabled: true,
                textSize: 14,
                form: "SQUARE",
                formSize: 14,
                xEntrySpace: 10,
                yEntrySpace: 5,
                wordWrapEnabled: true
            },
            data: {
                dataSets: [{
                    values: this.chartY,
                    label: '',
                    config: {
                        colors: [processColor('#726969'), processColor('red')],
                        stackLabels: ['Đã sử dụng', 'Tốt'],
                        barSpacePercent: 0,
                        valueTextColor: processColor('#fff')
                    }
                }],
            },
            xAxis: {
                valueFormatter: this.chartX,
                granularityEnabled: true,
                granularity: 1,
                labelCount: 10,
                position: 'BOTTOM',
                labelRotationAngle: -60,
                textColor: processColor(Configs.COLOR_ITEM),

            }

        };

    }
    handleSelect(event) {
        let entry = event.nativeEvent
        if (entry == null) {
            this.setState({ ...this.state, selectedEntry: null })
        } else {
            this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
        }
        Actions.carDetailInforChart({ data: entry })
    }
    componentDidMount() {
        //  alert(this.sta.isEmptyXLabel())

    }

    render() {
        return (
            <View style={{ width: width - 10, height: width, marginTop: 10, marginBottom: 60 }}>
                <View style={{ flex: 1 }}>
                    <View style={[styles.container, { elevation: Configs.ELEVATION, marginBottom: 5, borderWidth: 1, borderColor: '#f0f0f0' }]}>
                        <View style={{
                            marginLeft: 5,
                            marginRight: width / 2,
                            marginTop: 2,
                            padding: 5,
                            backgroundColor: '#f0f0f0',
                            borderRadius: 4,
                            alignItems: 'center', justifyContent: 'center',
                            elevation: Configs.ELEVATION,
                        }}>
                            <TextW> Chi Tiết</TextW>
                        </View>
                        <BarChart
                            ref={(refs) => this.sta = refs}
                            style={styles.chart}
                            xAxis={this.state.xAxis}
                            yAxis={{
                                left: {
                                    valueFormatter: '###',
                                    textColor: processColor(Configs.COLOR_ITEM),
                                    axisMinimum: 0,
                                    spaceBottom: 0,
                                    spaceTop: 0,
                                    labelCount: 5,
                                    labelCountForce: true,
                                    granularity: 111,
                                    granularityEnabled: true,
                                },
                                right: {
                                    enabled: false
                                }
                            }
                            }
                            chartDescription={{ text: '' }}
                            data={this.state.data}
                            legend={this.state.legend}
                            drawValueAboveBar={false}
                            marker={{
                                enabled: false,
                                markerColor: processColor(Configs.COLOR_PRIMARY),
                                textColor: processColor('white'),
                                markerFontSize: 14,
                            }}
                            onSelect={this.handleSelect.bind(this)}
                            dragDecelerationEnabled={true}
                            dragDecelerationFrictionCoef={0.99}
                        />
                    </View>
                </View>

            </View>
        );
    }

}
