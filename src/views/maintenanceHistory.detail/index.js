'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Layout from './layout';

class MaintainHistoryDetail extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            statusCheck: false,
            statusChange: false,
        }
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(MaintainHistoryDetail);
