'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Switch } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import { TextViewPrimaryBold, TextW } from '../../view_components/text';
import { Header } from '../../view_components/header';
import Icon from 'react-native-vector-icons/FontAwesome';
import StarRating from 'react-native-star-rating';
// import { Switch } from 'react-native-switch';
import _ from 'lodash';
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Lịch sử bảo trì chi tiết'} />
                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                    {this.renderDetail()}
                    {this.renderRating()}
                    {this.renderInfor()}
                </ScrollView>
            </View>
        );
    }
    renderDetail() {
        let strtime = this.state.data.update_time;
        let dateformat = "";
        try {
            let res = strtime.split(" ");
            let date = new Date(res[0])
            dateformat = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear()+" "+res[1];
        } catch (error) {
            dateformat = "--/--/----"
        }
        return (
            <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ededed' }}>
                <View>
                    <Text style={{ color: Configs.COLOR_PRIMARY, fontSize: 18, fontWeight: 'bold' }}>{this.state.data.maintenance_schedule_name} - Tại {this.state.data.distance_check} Km</Text>
                    {this.renderTextIcon('calendar-check-o', 'Bảo trì: Thay thế, kiểm tra và lau chùi')}
                    {this.renderTextIcon('refresh', 'Time: ' + dateformat)}
                    {this.renderTextIcon('leaf','Head: ' + this.state.data.maintenance_center_name)}
                </View>
            </View>
        );
    }
    renderTextIcon(nameIcon, txt) {
        return (
            <View style={{ padding: 5, flexDirection: 'row' }}>
                <View style={{ padding: 5, borderRadius: 100, backgroundColor: Configs.COLOR_PRIMARY }}>
                    <Icon name={nameIcon} color='#fff' size={12} />
                </View>
                <Text style={{ marginLeft: 5 }}>{txt}</Text>
            </View>
        );

    }
    renderRating() {
        return (
            <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Mức độ hài lòng của bạn </Text>
                <StarRating
                    disabled={true}
                    maxStars={5}
                    rating={this.state.data.rank}
                    starSize={20}
                    starColor={'#ffd203'}
                    selectedStar={(rating) => { alert(rating) }}
                />
            </View>
        );
    }
    renderInfor() {
        let value = _.groupBy(this.state.data.list_item_maintenance, 'maintenance_type');
        return (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ marginLeft: 10, color: 'black', fontSize: 18 }}>Các mục bảo trì</Text>
                {value[0]?this.renderItemPri('calendar-check-o', 'Kiểm tra lau chùi ' + value[0].length + ' hạng mục', this.state.data.maintenance_check == '0' ? 'Chưa hoàn thành' : 'Hoàn thành', this.state.data.maintenance_check == '0' ? false : true, () => { Actions.maintenanceList({ data: value[0], icon: 'calendar-check-o', title: 'Kiểm tra lau chùi' }) }, () => { this.setState({ statusCheck: !this.state.statusCheck }) }):null}
                {value[1]?this.renderItemPri('refresh', 'Thay ' + value[1].length + ' hạng mục', this.state.data.maintenance_replace == '0' ? 'Chưa hoàn thành' : 'Hoàn thành', this.state.data.maintenance_replace == '0' ? false : true, () => { Actions.maintenanceList({ data: value[1], icon: 'refresh', title: 'Thay thế' }) }, () => { this.setState({ statusChange: !this.state.statusChange }) }):null}
                {/* {this.renderItemPri('leaf', 'Lau chùi 22 hạng mục', 'Chưa hoàn thành', false, () => { Actions.maintenanceList() })} */}
            </View>
        );
    }
    renderItemPri(icon, name, status, isSuccess, action, actionStatus) {
        return (
            <View style={{ flex: 1, backgroundColor: '#ededed', padding: 10, flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: Configs.COLOR_PRIMARY, padding: 10, borderRadius: 100 }}>
                        <Icon size={25} color={'#fff'} name={icon} />
                    </View>
                </View>
                <View style={{ flex: 3 }}>
                    <TouchableOpacity style={{ flex: 1, flexDirection: 'row', padding: 5 }} onPress={action}>
                        <View style={{ flex: 1 }}>
                            <Text>{name}</Text>
                        </View>
                        <Text style={{ fontSize: 10, color: Configs.COLOR_PRIMARY }}>Chi tiết...</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'row', borderTopColor: '#fff', borderTopWidth: 1, padding: 5 }}>
                        <View style={{ flex: 1 }}>
                            <Text>{status}</Text>
                        </View>
                        <Switch
                            value={isSuccess}
                            disabled={true}
                            onValueChange={actionStatus}
                            thumbTintColor={Configs.COLOR_PRIMARY}
                            onTintColor={'#f4bbc0'}
                        />
                    </View>
                </View>
            </View>
        )
    }
}
