'use strict';

import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import StatusBarHeight, { HR } from '../../view_components/statusbarHeight';
import { Actions } from 'react-native-router-flux';
import Configs from '../../configs';
import style from './style';
const ICON_MOTO = require('../../resources/moto.png')
export default class Layout extends Component {

    render() {
        return (
            <View style={
                [style.container,
                {
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#fff'
                }]}>
                <StatusBarHeight />
                <View style={{ flex: 0.5, justifyContent: 'center' }}>
                    <Text style={[style.txtBTN,{color:'red'}]}>Bảo Trì Xe Máy</Text>
                </View>

                <View style={{ flex: 1 }}>
                    <Image
                        style={style.img}
                        source={ICON_MOTO}
                    />
                </View>

                <TouchableOpacity
                    activeOpacity={.8}
                    style={style.btn}
                    onPress={() => { Actions.login({register:true}) }}
                >
                    <Text style={[style.txtBTN,{fontSize:18}]}>Tạo tài khoản mới</Text>
                </TouchableOpacity>
                <HR />
                <TouchableOpacity
                    activeOpacity={.8}
                    style={style.btn}
                    onPress={() => {  Actions.login() }}
                >
                    <Text style={[style.txtBTN,{fontSize:18}]}>Đăng Nhập</Text>
                </TouchableOpacity>
                <View style={{ height: 50, justifyContent: 'flex-end', alignItems: 'center' }}>

                </View>
            </View>
        );
    }
}
