'use strict';

import { StyleSheet,Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    btn:{ width: width, padding: 15, alignItems: 'center', backgroundColor: '#d42032' },
    txtBTN:{ fontSize: 25,color:'#fff' },
    img:{ width: width / 2, height: width / 2 }
});

export default styles;
