'use strict';

import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textInput: {
        width: width * .8,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#fff',
        fontSize: 16,
        padding: 5,
        marginBottom: 20,
        color: '#fff',
    },
    btnForgot: {
        width: width * .8,
        padding: 5,
        justifyContent: 'center', alignItems: 'center',
        backgroundColor: 'transparent'
    },
    btnSignin: {
        width: width * .8,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20
    },
    txtHeader: { fontSize: 20, color: '#d42032', fontWeight: 'bold' },
    txtForgot: { fontSize: 18, color: '#fff', fontWeight: 'bold' }
});

export default styles;
