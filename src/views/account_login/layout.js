'use strict';

import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator,Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Header } from '../../view_components/header';
import DropdownAlert from 'react-native-dropdownalert'
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from '../../view_components/rn-viewpager';
import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import RegisterCBN from '../account_register';

export default class Layout extends Component {


    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Đăng Nhập/Đăng kí'} />
                <Animated.View style={[style.container, { }]}>
                    <IndicatorViewPager
                        style={{ flex: 1, backgroundColor: '#fff' }}
                        initialPage={this.props.register?1:0}
                        indicator={this._renderTitleIndicator()}
                    >
                        <View style={{flex:1, backgroundColor: '#fff' }}>
                             {this.renderLogin()}

                </View>
                        <View style={{flex:1, backgroundColor: '#fff' }}>
                            {/* {this.renderHistory()} */}
                            <RegisterCBN />
                        </View>
                    </IndicatorViewPager>
                </Animated.View>

                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false}
                    translucent={true}
                    style={{ paddingTop:20,padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }
    _renderTitleIndicator() {
        return <PagerTitleIndicator
            itemStyle={{ borderWidth: 1, borderColor: Configs.COLOR_PRIMARY, margin: 5, borderRadius: 10 }}
            itemTextStyle={{ color: Configs.COLOR_ITEM }}

            selectedItemStyle={{ borderWidth: 1, borderColor: Configs.COLOR_PRIMARY,backgroundColor:Configs.COLOR_ITEM, margin: 5, borderRadius: 10 }}
            selectedItemTextStyle={{ color: '#fff' }}

            titles={['Đăng nhập','Đăng kí' ]}
        />;
    }
    renderLogin() {
        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <View style={{ flex: .3, alignItems: 'center' }}>
                    <Text style={style.txtHeader}>Sign In</Text>
                </View>

                <View style={{ flex: 1, alignItems: 'center' }}>
                    {this.renderTextInput('Email/Phone')}
                    <TouchableOpacity
                        style={[style.btnSignin, { backgroundColor: this.state.disableBTN ? Configs.COLOR_ITEM : '#d42032' }]}
                        onPress={() => { this._onClickLogin() }}
                        disabled={this.state.disableBTN}
                    >
                        {this.state.disableBTN ?
                            <ActivityIndicator size={'small'} />
                            :
                            <Text style={[style.txtHeader,{color:'#fff'}]}>Đăng Nhập</Text>
                        }

                    </TouchableOpacity>

                </View>
            </View>

        );
    }
    renderTextInput(placeHolder) {
        return (
            <TextInput
                style={[style.textInput, { backgroundColor: '#d42032', paddingLeft: 10 }]}
                placeholderTextColor='#fff'
                underlineColorAndroid={'transparent'}
                placeholder={placeHolder}
                value={this.state.email}
                autoFocus={true}
                onChangeText={(val) => { this.setState({ email: val }) }}
            />
        );
    }
}
