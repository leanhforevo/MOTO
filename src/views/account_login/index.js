'use strict';
import { Keyboard } from 'react-native';
import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import DataAPI from '../../utils/fetchData';
import Configs from '../../configs'
import { Actions } from 'react-native-router-flux';

import Layout from './layout';

class Login extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            disableBTN: false
        }
    }

    _onClickLogin() {
        this.setState({ disableBTN: !this.state.disableBTN });
        let url = Configs.API + '/api/user/login?key=' + Configs.keyAPI + '&login=' + this.state.email;
        DataAPI.fetchDataAPI(url, (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.setState({ disableBTN: !this.state.disableBTN });
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {
                    this.props.actions.app.setAccountInfor(data)
                        .then(() => {
                            Actions.mainCar({ type: 'replace' });
                        })
                }
            } else {
                this.setState({ disableBTN: !this.state.disableBTN });
            }
        })
    }

}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(Login);
