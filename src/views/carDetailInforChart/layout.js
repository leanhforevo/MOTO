'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import { Header } from '../../view_components/header';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';

const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
        //    alert(JSON.stringify(props.data))
        this.state = {
            data: props.data
        }
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Tình trạng hiện tại'} />
                {this.renderNow()}
            </View>
        );
    }
    renderNow() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff', padding: 5, marginTop: 5 }}>
                <View style={{ borderRadius: 4 }}>
                    <View style={{ padding: 5, justifyContent: 'flex-start' }}>
                        <View style={{
                            marginLeft: 5,
                            marginRight: width / 2,
                            marginTop: 2,
                            padding: 5,
                            backgroundColor: '#ededed',
                            borderRadius: 4,
                            alignItems: 'center', justifyContent: 'center'
                        }}>
                            <TextW> Detail</TextW>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', padding: 5 }}>
                        <View style={{ alignItems: 'center' }}>
                            {/* <TextView style={{ color: 'red' }}>{this.state.data.yValues[1]}%</TextView> */}
                            <View style={{ width: 50, height: width, backgroundColor: 'red', justifyContent: 'flex-end' }} >
                                <View style={{ width: 50, height: width * (this.state.data.yValues[0] / 100), backgroundColor: '#726969', justifyContent: 'center' }} >
                                    <TextW style={{ transform: [{ rotate: '-90deg' }], color: '#fff' }}>Used {this.state.data.yValues[0].toFixed(2)}%</TextW>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 1 }}>
                            {this.renderItem()}
                        </View>

                    </View>

                </View>
                {this.renderBTNBook()}
            </View >
        );
    }
    renderBTNBook() {
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: Configs.COLOR_ITEM,
                    borderRadius: 4,
                    // width: 70,
                    // height: 50,
                    position: 'absolute',
                    bottom: 0,
                    right: 0,
                    left: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: width * .2,
                    marginLeft: width * .2,
                    marginBottom: 20,
                    padding: 10, elevation: 2
                }}
                onPress={() => { alert('press') }}
                activeOpacity={.8}
            >
                <TextW style={{ color: '#fff' }}>Đặt lịch kiểm tra</TextW>
            </TouchableOpacity>
        );
    }
    renderItem() {
        return (
            <View>

                <View style={{ padding: 5 }}>
                    <View style={{
                        borderRadius: 4,
                        borderWidth: 1,
                        borderColor: Configs.COLOR_ITEM,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <TextViewPrimaryBold>Lần Bảo Trì Gần Nhất</TextViewPrimaryBold>
                        <View style={{ padding: 10 }}>
                            <Text>Thay : ngay 20/20/2000 ở 4000Km</Text>
                            <Text>Head : cmt8 - hcm</Text>
                            <Text>Đã sử dụng : 3400 Km</Text>
                        </View>
                    </View>

                </View>

                <View style={{ padding: 5 }}>
                    <View style={{
                        borderRadius: 4,
                        borderWidth: 1,
                        borderColor: Configs.COLOR_ITEM,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <TextViewPrimaryBold>Hiện Tại</TextViewPrimaryBold>
                        <View style={{ padding: 10 }}>
                            <Text>Có thể sử dụng : 600Km</Text>
                            <Text>Đề nghị : Nên thay càng sớm càng tốt</Text>
                            <Text>Có thể sử dụng : 600Km</Text>
                            <Text>Đề nghị : Nên thay càng sớm càng tốt</Text>
                        </View>

                    </View>

                </View>
            </View>
        );
    }
    renderHeader() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }} onPress={() => { Actions.pop() }}>
                    <Icon name='reply' size={30} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: Configs.COLOR_ITEM }}>VESPA 2017</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    <Icon name='cog' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>

            </View>
        );
    }


}
