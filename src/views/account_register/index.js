'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from './layout';
import { Actions } from 'react-native-router-flux';
import Configs from '../../configs';
import DataAPI from '../../utils/fetchData';

class Register extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            gender: 0,
            name: '',
            email: '',
            phone: '',
            loadbtn: false
        }
    }
    checkValidate() {
        var regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        var phoneno = /^\d+$/;
        //check null
        if (this.state.name == '' && this.state.email == '' && this.state.phone == '') {
            alert('Please input all Field!')
            return false;
        } else if (this.state.name.length < 5) {
            alert('Name must more than 5 character!'); return false;
        } else if (this.state.email.length < 5 && !regexEmail.test(this.state.email)) {
            alert('Email must more than 5 character and format type Email'); return false;
        } else if (this.state.phone.length < 8) {

            alert('Phone must more than 8 character'); return false;
        } else {
            if (!phoneno.test(this.state.phone)) {
                alert('Phone must is number'); return false;
            }
            return true;
        }
    }
    async registerAccount() {
        this.setState({ loadbtn: true })
        if (this.checkValidate) {
            await this._onClickLogin();
        } else {
            this.setState({ loadbtn: false })
        }

    }
    _onClickLogin() {
        let url = Configs.API + '/api/user/register?key=' + Configs.keyAPI + '&name=' + this.state.name + '&gender=' + this.state.gender + '&tel=' + this.state.phone + '&email=' + this.state.email + '';
        DataAPI.fetchDataAPI(url, (data) => {
            if (data) {
                console.log('------------------------------------');
                console.log(data);
                console.log('------------------------------------');
                console.log('-----:'+url);
                if (data.statusCode != '000') {
                    this.setState({ loadbtn: false });
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {
                    this.props.actions.app.setAccountInfor(data)
                        .then(() => {
                            this.setState({ loadbtn: false });
                            Actions.mainCar({ type: 'replace' });
                        })
                }
            } else {
                this.setState({ loadbtn: false });
            }
        })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(Register);
