'use strict';

import { StyleSheet, Dimensions } from 'react-native';
import Configs from '../../configs';
const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textInput: {
        width: width * .8,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#40669d',
        fontSize: 16,
        padding: 5, marginBottom: 10, backgroundColor: '#fff',color:Configs.COLOR_ITEM
    },
    txtHeader: {
        fontSize: 20,
        color: '#fff'
    },
    btnSubmit: {
        backgroundColor: Configs.COLOR_ITEM,
        padding: 10,
        alignItems: 'center',
        width: width,
        justifyContent: 'center'
    }
});

export default styles;
