'use strict';

import React, { Component } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, Dimensions, ActivityIndicator } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Configs from '../../configs';
import style from './style';
import DropdownAlert from 'react-native-dropdownalert'
import StatusBarHeight, { HR } from '../../view_components/statusbarHeight';
export default class Layout extends Component {


    render() {
        var radio_props = [
            { label: 'Nam', value: 0 },
            { label: 'Nữ', value: 1 }
        ];
        return (
            <View style={[style.container, { backgroundColor: '#ffff' }]}>
                <StatusBarHeight />
                <ScrollView>
                    <View style={[style.container, { alignItems: 'center' }]}>
                        <Text style={[style.txtHeader, { marginBottom: 30, color: '#d42032' }]}>Đăng kí tài khoản</Text>
                        <Text style={[style.txtHeader, { marginBottom: 30, color: '#d42032' }]}>Điền thông tin của bạn</Text>
                        {this.renderTextInput('Name', (val) => { this.setState({ name: val }) })}
                        <RadioForm
                            radio_props={radio_props}
                            initial={0}
                            formHorizontal={true}
                            onPress={(value) => { this.setState({ gender: value }) }}
                            buttonColor='#d42032'
                            radioStyle={{
                                width: Dimensions.get('window').width * .4,
                                alignItems: 'flex-start',
                                marginBottom: 10, marginTop: 10, paddingLeft: 10
                            }}
                        />
                        {this.renderTextInput('Email Address', (val) => { this.setState({ email: val }) })}
                        {this.renderTextInput('Phone Number', (val) => { this.setState({ phone: val }) })}
                    </View>
                </ScrollView>

                <HR />
                <TouchableOpacity
                    style={style.btnSubmit}
                    activeOpacity={0.8} onPress={() => this.registerAccount()} disabled={this.state.loadbtn}>
                    {this.state.loadbtn ?
                        <ActivityIndicator size={'small'} style={{ margin: 10 }} />
                        :
                        <Text style={style.txtHeader}>Next</Text>
                    }
                </TouchableOpacity>
                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false} //translucent={true}
                    containerStyle={{ padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }
    renderTextInput(placeHolder, action) {
        return (
            <TextInput
                style={style.textInput}
                underlineColorAndroid={'transparent'}
                placeholder={placeHolder}
                placeholderTextColor={Configs.COLOR_ITEM}
                onChangeText={action}
            />
        );
    }
}
