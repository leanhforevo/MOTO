'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';

import {
    Platform,
    NetInfo,
    AppState,
    Linking,
    StatusBar,
    BackHandler,
    ToastAndroid
} from 'react-native';


import Logs from '../../utils/logs';
import Configs from '../../configs';

import Layout from './layout';

class Launcher extends Layout {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.loadData();
    }
    loadData() {
        this.props.actions.app.getAccountInfor()
            .then(() => {
                if (this.props.app.accountInfor && this.props.app.accountInfor.user_id) {
                    Actions.mainCar({ type: 'replace' });
                } else {
                    Actions.home({ type: 'replace' });
                }
            })

    }

}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(Launcher);
