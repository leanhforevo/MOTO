'use strict';

import { StyleSheet } from 'react-native';
import Scale from '../../utils/scale';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    text: {
        fontSize: Scale.scaleWidthDecOne_Standard(34),
        color: 'rgba(48, 48, 48, 1)'
    }
});

export default styles;
