'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';

import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';
import { Header } from '../../view_components/header';
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Thông tin tài khoản'} />
                <View style={{ flex: 1 }}>
                    <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <TextView style={{ fontSize: 18 }}>Mời bạn nhập thông tin mới</TextView>
                    </View>
                    <View style={{ flex: 1 }}>
                        {this.renderItemAcc('ID', this.state.accountInfor.user_id)}
                        {this.renderItemAcc('Họ và Tên', this.state.accountInfor.name)}
                        {this.renderItemAccSex('Giới tính', this.state.accountInfor.gender)}
                        {this.renderItemAcc('Số điện thoại', this.state.accountInfor.tel)}
                        {this.renderItemAcc('Email', this.state.accountInfor.email)}
                    </View>
                    <View style={{ height: 100, alignItems: 'center' }}>
                        <TouchableOpacity style={{ padding: 10, elevation: 4, width: width / 2, backgroundColor: Configs.COLOR_PRIMARY, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                            <TextView style={{ color: '#fff' }}>Lưu mới</TextView>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 100, alignItems: 'center' }}>
                        <TouchableOpacity
                        style={{ padding: 10, elevation: 4, width: width / 2,
                         backgroundColor: Configs.COLOR_PRIMARY, borderRadius: 4,
                          justifyContent: 'center', alignItems: 'center' }}
                          onPress={()=>{this.logOut()}}>
                            <TextView style={{ color: '#fff' }}>Đăng xuất.</TextView>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
    renderItemAcc(title, infor) {
        return (
            <View style={{ flexDirection: 'row', borderWidth: .5, borderColor: '#e0e0e0', marginBottom: 10 }}>
                <View style={{ flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ececec' }}>
                    <TextW style={{}}>{title}</TextW>
                </View>
                <View style={{ flex: 2, padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <TextView>{infor}</TextView>
                </View>
            </View>
        );
    }
    renderItemAccSex(title, infor) {
        var radio_props = [
            { label: 'Nam', value: 0 },
            { label: 'Nữ', value: 1 }
        ];
        return (
            <View style={{ flexDirection: 'row', borderWidth: .5, borderColor: '#e0e0e0', marginBottom: 10 }}>
                <View style={{ flex: 1, padding: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ececec' }}>
                    <TextW style={{}}>{title}</TextW>
                </View>
                <View style={{ flex: 2, padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <RadioForm
                        radio_props={radio_props}
                        initial={parseInt(infor)}
                        formHorizontal={true}
                        onPress={(value) => { this.setState({ gender: value }) }}
                        buttonColor='#d42032'
                        radioStyle={{
                            width: Dimensions.get('window').width/3*2 * .4,
                            alignItems: 'flex-start',
                            marginBottom: 10, marginTop: 10, paddingLeft: 10
                        }}
                    />
                </View>
            </View>
        );
    }
}
