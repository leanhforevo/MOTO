'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux'
import Layout from './layout';

class AccountInformation extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            accountInfor: props.app.accountInfor
        }
    }
   async logOut() {
        this.props.actions.app.removeAccount()
            .then(() => {
                this.props.actions.app.removeBike()
                    .then(() => {
                        this.props.actions.app.getAccountInfor()
                       .then(()=>{
                           Actions.launcher();
                       })
                    })
            })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(AccountInformation);
