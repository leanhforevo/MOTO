'use strict';

import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    chart: {
        flex: 1
    },
    indicatorContainer: {
        backgroundColor: 0xFFFFFFFF,
        borderTopWidth: 0,
        height: 56,
        paddingTop: 0,
        paddingBottom: 0
    },
    tabIcon: {
        width: 20,
        height: 20,
        tintColor: '#7F8C8D',
        resizeMode: 'contain'
    },
    selectedTabIcon: {
        width: 20,
        height: 20,
        tintColor: '#2C3E50',
        resizeMode: 'contain'
    },
    tabTxt: {
        color: '#34495E',
        marginTop: 0,
        fontSize: 10.5
    },
    selectedTabTxt: {
        color: '#2C3E50',
        marginTop: 0,
        fontSize: 12
    },
    tabItem: {
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 8
    },
    selectedTabItem: {
        justifyContent: 'space-between',
        paddingBottom: 10,
        paddingTop: 6
    }
});

export default styles;
