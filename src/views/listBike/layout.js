'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, ActivityIndicator, FlatList, RefreshControl } from 'react-native';
import { Actions } from 'react-native-router-flux';
import DropdownAlert from 'react-native-dropdownalert'
import Configs from '../../configs';
import style from './style';
import _ from 'lodash';
import StatusBarHeight from '../../view_components/statusbarHeight';
import {HeaderTitle} from '../../view_components/header';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');
export default class LayoutListBike extends Component {

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                 <HeaderTitle title={'Bảo Trì Xe Máy'} />
                {this.state.loaded && this.props.app.listBike.statusCode ?
                    <View style={{ flex: 1, padding: 10, paddingBottom: 50 }}>
                        <FlatList
                            refreshControl={
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={() => { this.fetchDataListVerhicle() }}
                                />
                            }
                            showsVerticalScrollIndicator={false}
                            style={{}}
                            data={_.orderBy(this.props.app.listBike.items, ['id'], ['asc'])}
                            keyExtractor={(value, index) => index}
                            renderItem={(value) => this.renderItemCar(value)}
                        />
                    </View>
                    :
                    <ScrollView
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => { this.fetchDataListVerhicle() }}
                            />
                        }
                        style={{
                            marginTop: (height - 70) / 2
                        }}>
                        <ActivityIndicator size={'large'} />
                    </ScrollView>
                }
                {this.renderBTNAdd()}
                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false}
                    containerStyle={{ padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }

    renderBTNAdd() {
        return (
            <TouchableOpacity
                style={{
                    backgroundColor: Configs.COLOR_ITEM,
                    borderRadius: 4,
                    // width: 70,
                    // height: 50,
                    position: 'absolute',
                    bottom: 0,
                    right: 0,
                    left: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: width * .2,
                    marginLeft: width * .2,
                    marginBottom: 20,
                    padding: 10
                }}
                onPress={() => { Actions.mainCarAdd() }}
                activeOpacity={.8}
            >
                <Icon name='plus' color={'#fff'} size={20} />
            </TouchableOpacity>
        );
    }
    renderItemCar(item) {
        return (
            <View style={{ borderColor: Configs.COLOR_PRIMARY,
             borderBottomWidth: 1, marginBottom: 5,
            // elevation: Configs.ELEVATION,
              }}>
                <TouchableOpacity
                    key={item.index}
                    style={{ padding: 10, flexDirection: 'row' }}
                    onPress={() => {
                        this._pressedItem(item.item)
                        // Actions.drawer({ bikeSelected: item.item })
                    }}
                    activeOpacity={.8}
                >
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
                        <Icon name='motorcycle' size={60} color={Configs.COLOR_ITEM} />
                    </View>
                    <View style={{ flex: 2, justifyContent: 'center', }}>
                        <Text style={{ color: Configs.COLOR_ITEM, fontSize: 18 }}>{item.item.name}</Text>
                    </View>
                </TouchableOpacity>
            </View>

        );
    }
}
