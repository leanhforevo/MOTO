'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import DataAPI from '../../utils/fetchData';
import Layout from './layout';
import Configs from '../../configs';
import { Actions } from 'react-native-router-flux';

import {
    Platform,
    NetInfo,
    AppState,
    Linking,
    StatusBar,
    BackHandler,
    ToastAndroid
} from 'react-native';

class ListBike extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            data: {},
            refreshing: false
        }
    }
    componentWillMount() {
        if (Platform.OS == "android") {
            this.backHandlerE = BackHandler.addEventListener('hardwareBackPress', () => {
                let now = new Date().getTime();
                if (this.lastPress && (now - this.lastPress) <= 2000) {
                    BackHandler.exitApp(0)
                    delete this.lastPress;
                    return true;
                } else {
                    this.lastPress = now
                    ToastAndroid.show('Nhấn lần nữa để thoát !', ToastAndroid.SHORT);
                    return true;
                }
            })
        }
    }
    componentWillUnmount() {
        if (Platform.OS == "android") {
            this.backHandlerE.remove();
        }
    }
    componentDidMount() {
        this.props.actions.app.getBike()
            .then(() => {
                if (this.props.app.listBike && this.props.app.listBike.statusCode) {
                    // console.log(this.props.app.listBike)
                    this.setState({ loaded: true, data: this.props.app.listBike })
                    this.fetchDataListVerhicle();
                } else {
                    this.fetchDataListVerhicle();
                }
            })
    }
    componentWillReceiveProps() {
        if (this.props.addRF) this.fetchDataListVerhicle();
    }
    fetchDataListVerhicle() {
        this.setState({ refreshing: false })
        let url = Configs.API + '/api/vehicle/user-vehicle?key=' + Configs.keyAPI + '&user_id=' + this.props.app.accountInfor.user_id;
        DataAPI.fetchDataAPI(url, (data) => {
            //  alert(JSON.stringify(data))
            if (data) {

                if (data.statusCode != '000') {
                    //   this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                    this.setState({ loaded: true, data: data, refreshing: false })
                    Actions.mainCarAdd();
                } else {
                    this.props.actions.app.setBike(data)
                        .then(() => {
                            this.setState({ loaded: true, data: data, refreshing: false })
                        })
                }
            }
        })
    }
    _pressedItem(item) {
        this.props.actions.app.setBikeSelected(item)
        Actions.drawer();
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(ListBike);
