'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Alert, Switch, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight, { Time } from '../../view_components/statusbarHeight';
import { TextViewPrimaryBold, TextW } from '../../view_components/text';
import { Header } from '../../view_components/header';
import Icon from 'react-native-vector-icons/FontAwesome';
import StarRating from 'react-native-star-rating';
// import { Switch } from 'react-native-switch';
import DropdownAlert from 'react-native-dropdownalert'
import _ from 'lodash';
import DateTimePicker from 'react-native-modal-datetime-picker';
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <Header title={'Lịch bảo trì chi tiết'} right={() => {
                    Alert.alert(
                        'Thông báo.',
                        'Xác nhận lưu thông tin.',
                        [
                            { text: 'Hủy.', onPress: () => console.log('Cancle') },
                            { text: 'Lưu.', onPress: () => this._onSaveHead() },
                        ],
                        { cancelable: false }
                    )
                }} />
                <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                    {this.renderDetail()}
                    {this.renderRating()}
                    {this.renderInfor()}
                </ScrollView>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    mode={'datetime'}
                    // datePickerModeAndroid={'spinner'}
                    minimumDate={new Date()}
                />
                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false}
                    translucent={true}
                    style={{ paddingTop: 20, padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }
    renderDetail() {
        return (
            <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ededed' }}>
                <View>
                    <TextViewPrimaryBold>{this.state.data.maintenance_schedule_name} - {this.state.data.distance_check} Km</TextViewPrimaryBold>
                    <Text>Lau chui, thay va kiem tra</Text>
                </View>

                {this.renderDetailSelectHead()}
                {this.renderDetailSelectTime()}


            </View>
        );
    }
    renderDetailSelectHead() {
        return (
            <TouchableOpacity
                activeOpacity={.8}
                onPress={() => { this._onGetHead() }}
                style={{
                    flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',
                    marginTop: 5
                }}
                disabled={!this.state.btnHead}>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: Configs.COLOR_PRIMARY }}>Chọn Head bảo trì: </Text>
                </View>

                <View style={{ flex: 1, borderWidth: 0.5, borderColor: 'grey', flexDirection: 'row', width: width / 3, padding: 2 }}>
                    {!this.state.btnHead ?
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                            <ActivityIndicator s size={'small'} />
                        </View>
                        :
                        <View style={{ flex: 1 }}>
                            <Text style={{ color: Configs.COLOR_PRIMARY }}>{this.state.headSelected ? this.state.headSelected : "Chọn head"} </Text>
                        </View>
                    }
                    <Icon name={'search'} size={18} color={Configs.COLOR_PRIMARY} />
                </View>
            </TouchableOpacity>
        );
    }
    renderDetailSelectTime() {
        return (
            <TouchableOpacity onPress={this._showDateTimePicker} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: Configs.COLOR_PRIMARY }}>Chọn thời gian: </Text>
                </View>
                <View style={{ flex: 1, borderWidth: 0.5, borderColor: 'grey', flexDirection: 'row', width: width / 3, padding: 2 }}>
                    <View style={{ flex: 1 }}>
                        <Time style={{ color: Configs.COLOR_PRIMARY }} time={this.state.timePick} />
                    </View>
                    <Icon name={'search'} size={18} color={Configs.COLOR_PRIMARY} />
                </View>
            </TouchableOpacity>
        );
    }
    renderRating() {
        return (
            <View style={{ padding: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Text>Mức độ hài lòng của bạn </Text>
                <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={this.state.rating}
                    starSize={20}
                    starColor={'#ffd203'}
                    selectedStar={(rating) => { this.setState({ rating }) }}
                />
            </View>
        );
    }
    renderInfor() {
        let value = _.groupBy(this.state.data.list_item_maintenance, 'maintenance_type');
        return (
            <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                <Text style={{ marginLeft: 10, color: 'black', fontSize: 18 }}>Các mục bảo trì</Text>
                {value[0]?this.renderItemPri('calendar-check-o', 'Kiểm tra lau chùi ' + value[0].length + ' hạng mục', 'Hoàn thành', this.state.statusCheck, () => { Actions.maintenanceList({ data: value[0], icon: 'calendar-check-o', title: 'Kiểm tra lau chùi' }) }, () => { this.setState({ statusCheck: !this.state.statusCheck }) }):null}
                {value[1]?this.renderItemPri('refresh', 'Thay ' + value[1].length + ' hạng mục', 'Hoàn thành', this.state.statusChange, () => { Actions.maintenanceList({ data: value[1], icon: 'refresh', title: 'Thay thế' }) }, () => { this.setState({ statusChange: !this.state.statusChange }) }):null}
                {/* {this.renderItemPri('leaf', 'Lau chùi 22 hạng mục', 'Chưa hoàn thành', false, () => { Actions.maintenanceList() })} */}
            </View>
        );
    }
    renderItemPri(icon, name, status, isSuccess, action, actionStatus) {
        return (
            <View style={{ flex: 1, backgroundColor: '#ededed', padding: 10, flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ backgroundColor: Configs.COLOR_PRIMARY, padding: 10, borderRadius: 100 }}>
                        <Icon size={25} color={'#fff'} name={icon} />
                    </View>
                </View>
                <View style={{ flex: 3 }}>
                    <TouchableOpacity style={{ flex: 1, flexDirection: 'row', padding: 5 }} onPress={action}>
                        <View style={{ flex: 1 }}>
                            <Text>{name}</Text>
                        </View>
                        <Text style={{ fontSize: 10, color: Configs.COLOR_PRIMARY }}>Chi tiết...</Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1, flexDirection: 'row', borderTopColor: '#fff', borderTopWidth: 1, padding: 5 }}>
                        <View style={{ flex: 1 }}>
                            <Text>{status}</Text>
                        </View>
                        <Switch
                            value={isSuccess}
                            onValueChange={actionStatus}
                            thumbTintColor={Configs.COLOR_PRIMARY}
                            onTintColor={'#f4bbc0'}
                        />
                    </View>
                </View>
            </View>
        )
    }
}
