'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Layout from './layout';
import DataAPI from '../../utils/fetchData';
import Configs from '../../configs'
class MaintenanceDetail extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            isDateTimePickerVisible: false,
            timePick: '',
            statusCheck: false,
            statusChange: false,
            rating: 0,

            btnHead: true,
            arrHead: [],
            headSelected: "Chọn head",
            headSelectedID: "Chọn head",//id head

            btnSaving: false
        }
        console.log('------------------------------------');
        console.log(props);
        console.log('------------------------------------');
    }
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        // console.log('A date has been picked: ', date);
        this.setState({ timePick: date })
        this._hideDateTimePicker();
    };
    _onGetHead() {
        console.log('get Head')
        this.setState({ btnHead: false });
        let url = Configs.API + '/api/maintenance/maintenance-center?key=' + Configs.keyAPI + '&motorcycle_manufacturer_id=' + this.props.data.motorcycle_manufacturer_id;
        DataAPI.fetchDataAPI(url, (data) => {
            console.log('get Head', data)
            if (data) {
                if (data.statusCode != '000') {
                    this.setState({ btnHead: true });
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {

                    let arr = []
                    data.items.map((val, index) => {
                        let item = { id: val.maintenance_center_id, name: val.maintenance_center_name }
                        arr.push(item)
                    })
                    setTimeout(() => {
                        this.setState({ btnHead: true, arrHead: arr });
                        Actions.itemSelect({
                            data: arr,
                            title: 'Chọn Head bảo trì',
                            action: async (val) => {
                                let name = "Vui lòng chọn lại"
                                await data.items.map((value, index) => {
                                    if (value.maintenance_center_id == val) {
                                        name = value.maintenance_center_name
                                    }
                                });
                                this.setState({ headSelected: name, headSelectedID: val })
                            }
                        })
                    }, 10)
                }
            } else {
                this.setState({ btnHead: true });
            }
        })
    }
    _onSaveHead() {

        let stcheck = this.state.statusCheck ? 1 : 0;
        let stcleanreplace = this.state.statusChange ? 1 : 0;
        let url = Configs.API + '/api/maintenance/maintenance?key=' + Configs.keyAPI
        url += '&user_vehicle_id=' + this.props.app.bikeSelected.user_vehicle_id
        url += '&rank=' + this.state.rating
        url += '&maintenance_number=' + this.props.data.maintenance_number
        url += '&maintenance_check=' + stcheck
        url += '&maintenance_replace=' + stcleanreplace
        url += '&maintenance_center_id=' + this.state.headSelectedID;
        DataAPI.fetchDataAPI(url, async (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    // this.setState({ btnSaving: false });
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {
                    // this.setState({ btnSaving: false });
                    this.dropdown.alertWithType('success', 'SUCCESS', 'Lưu thành công.')
                    this.props.actionLoad()
                    setTimeout(() => {
                        Actions.pop();
                    }, 4000)
                }
            } else {
                // this.setState({ btnSaving: false });
                console.log('no data')
                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
            }
        })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(MaintenanceDetail);
