'use strict';

import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Layout from './layout';

class MaintainHistory extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            tag: this.props.app.bikeSelected.vehicle_type_name,
            loaded: false, refreshing: false,
            data: {}
        }
    }
    componentDidMount() {
        this.setState({ loaded: false, refreshing: true })
        this.props.actions.app.getMaintainHis(this.props.app.bikeSelected.user_vehicle_id)
            .then(() => {
                console.log('------------------------------------');
                console.log(this.props.app.maintainHis);
                console.log('------------------------------------');
                this.setState({ loaded: true, refreshing: false, data: this.props.app.maintainHis})
            })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(MaintainHistory);
