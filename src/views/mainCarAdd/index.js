'use strict';


import * as actions from '../../states/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Configs from '../../configs';
import DataAPI from '../../utils/fetchData';
import Layout from './layout';

class MainCarAdd extends Layout {
    constructor(props) {
        super(props);
        this.state = {
            arrManufacturer: [],
            itemManu: '',

            arrVehicle: [],
            itemVehicle: '',

            arrType: [],
            itemType: '',

            arrColor: [],
            itemColor: '',

            year: '',
            km: '',
            nameVehicle: '',
            license: '',

            data: [],
            loaded: false,
            disableBTN: false
        }
        //alert(JSON.stringify(this.props.app.accountInfor))
    }
    componentDidMount() {
        //  this.getManufactuer()
        this.getBikeAdd();
    }
    addVehicle() {
        this.setState({ disableBTN: true })
        let url = Configs.API + '/api/vehicle/register?key=' + Configs.keyAPI +
            '&user_id=' + this.props.app.accountInfor.user_id +
            '&motorcycle_manufacturer_id=' + this.state.itemManu +
            '&vehicle_type_id=' + this.state.itemVehicle +
            '&vehicle_sub_type_id=' + this.state.itemType +
            '&vehicle_color_id=' + this.state.itemColor +
            '&name=' + this.state.nameVehicle +
            '&year=' + this.state.year +
            '&current_km=' + this.state.km +
            '&license_plate=' + this.state.license;
        DataAPI.fetchDataAPI(url, (data) => {
            console.log(url)
            if (data) {
                if (data.statusCode == '000') {
                    this.dropdown.alertWithType('success', 'Add Vehicle', 'Thêm Xe Thành Công');
                    let url = Configs.API + '/api/vehicle/user-vehicle?key=' + Configs.keyAPI + '&user_id=' + this.props.app.accountInfor.user_id;
                    DataAPI.fetchDataAPI(url, (data) => {
                        //  alert(JSON.stringify(data))
                        if (data) {
                            if (data.statusCode != '000') {
                                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                                this.setState({ disableBTN: false })
                                // Actions.mainCarAdd();
                            } else {
                                this.props.actions.app.setBike(data)
                                    .then(() => {
                                        this.setState({ disableBTN: false });
                                        Actions.pop();
                                    })
                            }
                        }
                    })
                } else {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                    this.setState({ disableBTN: false })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                this.setState({ disableBTN: false })
            }
        })
    }
    getBikeAdd() {
        let url = Configs.API + '/api/vehicle/list-vehicle?key=' + Configs.keyAPI;
        DataAPI.fetchDataAPI(url, (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {
                    let arr = []
                    data.items.map((val, index) => {
                        let item = { id: val.motorcycle_manufacturer_id, name: val.motorcycle_manufacturer_name }
                        arr.push(item)
                    })
                    this.setState({ data: data, loaded: true, arrManufacturer: arr })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
            }
        })
    }
    getManufactuer() {
        let url = Configs.API + '/api/vehicle/motorcycle-manufacturer?key=' + Configs.keyAPI;
        DataAPI.fetchDataAPI(url, (data) => {
            if (data) {
                if (data.statusCode != '000') {
                    this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
                } else {
                    let arr = []
                    data.items.map((val, index) => {
                        let item = { id: val.motorcycle_manufacturer_id, name: val.name }
                        arr.push(item)
                    })
                    this.setState({ arrManufacturer: arr })
                }
            } else {
                this.dropdown.alertWithType('error', 'ERROR', data.alert_message ? data.alert_message : 'Lỗi')
            }
        })
    }
    getVerhicle() {
        try {
            let arr = []
            this.selectedManu = {}
            this.state.data.items.map((val, index) => { if (val.motorcycle_manufacturer_id == parseInt(this.state.itemManu)) { this.selectedManu = val } });
            this.selectedManu.vehicle_type.map((val, index) => {
                let item = { id: val.vehicle_type_id, name: val.vehicle_type_name }
                arr.push(item)
            })
            this.setState({ arrVehicle: arr })
        } catch (error) {
            this.dropdown.alertWithType('error', 'ERROR', 'Lỗi! Vui lòng thử lại.')
        }

    }
    getType() {
        try {
            let arr = []
            this.selectedVerhicle = {}
            this.selectedManu.vehicle_type.map((val, index) => { if (val.vehicle_type_id == parseInt(this.state.itemVehicle)) { this.selectedVerhicle = val } });

            this.selectedVerhicle.vehicle_sub_type.map((val, index) => {
                let item = { id: val.vehicle_sub_type_id, name: val.vehicle_sub_type_name }
                arr.push(item)
            })
            this.setState({ arrType: arr })
        } catch (error) {
            this.dropdown.alertWithType('error', 'ERROR', 'Lỗi! Vui lòng thử lại.')
        }

    }
    getColor() {

        let arr = []
        this.selectedType = {}
        this.selectedVerhicle.vehicle_sub_type.map((val, index) => { if (val.vehicle_sub_type_id == parseInt(this.state.itemType)) { this.selectedType = val } });


        this.selectedType.vehicle_color.map((val, index) => {
            let item = { id: val.vehicle_color_id, name: val.vehicle_color_name }
            arr.push(item)
        })
        this.setState({ arrColor: arr })
    }
}

const stateToProps = (state) => ({ ...state });
const dispatchToProps = (dispatch) => {
    let acts = {};
    for (let key in actions) {
        acts[key] = bindActionCreators(actions[key], dispatch);
    }
    return {
        actions: acts
    };
};
export default connect(stateToProps, dispatchToProps)(MainCarAdd);
