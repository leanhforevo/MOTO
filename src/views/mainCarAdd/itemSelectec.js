'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');
export default class Itemselect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.title ? this.props.title : 'Title',
            data: this.props.data ? this.props.data : []
        }
        // alert(props.data)
    }

    render() {
        return (
            <View style={[{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }]} >
                <TouchableOpacity activeOpacity={0} onPress={() => Actions.pop()} style={[{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: 'black', opacity: .4 }]} />
                <View style={{ width: width * .8, height: height * .8, backgroundColor: '#fff', borderRadius: 4, paddingBottom: 4 }}>
                    {this.renderItemTitle()}
                    {this.state.data.length <= 0 ?
                        <View />
                        :
                        <FlatList
                            data={this.state.data}
                            keyExtractor={(item, index) => index}
                            renderItem={(value, index) => this.renderItem(value.item, index)}
                        />
                    }
                </View>
            </View>
        );
    }
    renderItem(item, index) {
        //  alert(JSON.stringify(item))
        return (
            <View key={index} style={{ flex: 1, marginTop: 5, paddingLeft: 10, backgroundColor: '#fff' }}>

                <TouchableOpacity
                    onPress={() => { this.props.action(item.id); Actions.pop() }}
                    activeOpacity={.8}
                    style={{
                        height: 50, justifyContent: 'center', alignItems: 'center',
                        borderBottomWidth: 1, borderBottomColor: 'grey'
                    }}
                >
                    <Text>{item.name}</Text>
                </TouchableOpacity>
            </View>
        );
    }
    renderItemTitle() {
        return (
            <View style={{ height: 50, borderTopLeftRadius: 4, borderTopRightRadius: 4, backgroundColor: Configs.COLOR_ITEM, justifyContent: 'center', alignItems: 'center', borderBottomWidth: 1, borderBottomColor: 'grey' }}>
                <Text style={{ fontWeight: 'bold', fontSize: 18, color: '#fff' }}>Chọn {this.state.title}</Text>
            </View>
        );
    }
}