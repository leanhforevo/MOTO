'use strict';

import { StyleSheet,Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
import Configs from '../../configs';
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    itemContain:{
                width: width * .8,
                borderWidth: 1,
                borderColor: Configs.COLOR_ITEM,
                borderRadius: 3, padding: 5, alignItems: 'center', marginBottom: 20
            }
});

export default styles;
