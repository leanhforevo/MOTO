'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, TextInput, ScrollView, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import DataAPI from '../../utils/fetchData';
import Configs from '../../configs';
import style from './style';
import DropdownAlert from 'react-native-dropdownalert'
import _ from 'lodash';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                {this.renderHeader()}
                {this.state.data.statusCode ?
                    <View
                        style={{ flex: 1 }}
                    >
                        <View style={{ height: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, color: Configs.COLOR_ITEM }}>Chọn Thông Tin Xe Của Bạn</Text>
                        </View>
                        <View style={{ flex: 1, marginBottom: 50 }}>
                            <ScrollView >
                                {this.renderChoseItem()}
                            </ScrollView>
                        </View>
                        {this.renderBTNAdd()}
                    </View>
                    :
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <ActivityIndicator size={'large'} />
                    </View>
                }
                <DropdownAlert
                    ref={(ref) => this.dropdown = ref}
                    updateStatusBar={false}
                    translucent={true}
                    containerStyle={{ padding: 20, flexDirection: 'row' }}
                    onClose={(data) => { }} />
            </View>
        );
    }
    renderChoseItem() {
        return (
            <View style={{ flex: 5, alignItems: 'center', paddingTop: 50 }}>
                {this.renderItemTextInput('Nhập tên xe của bạn', this.state.nameVehicle, (val) => { this.setState({ nameVehicle: val }) }, 20)}
                {this.renderItem(
                    this.state.itemManu ? _.find(this.state.arrManufacturer, { id: this.state.itemManu }).name : 'Hãng Xe',
                    this.state.arrManufacturer,
                    async (id) => {
                        await this.setState({ itemManu: id, itemVehicle: '', itemType: '', itemColor: '' });
                        await this.getVerhicle(id);
                    },
                    this.state.arrManufacturer.length > 0 ? false : true
                )}
                {this.renderItem(
                    this.state.itemVehicle ? _.find(this.state.arrVehicle, { id: this.state.itemVehicle }).name : 'Loại Xe',
                    this.state.arrVehicle,
                    async (id) => {
                        await this.setState({ itemVehicle: id, itemType: '', itemColor: '' })
                        await this.getType();
                    },
                    this.state.arrVehicle.length > 0 ? false : true
                )}
                {this.renderItem(
                    this.state.itemType ? _.find(this.state.arrType, { id: this.state.itemType }).name : 'Phiên Bản',
                    this.state.arrType,
                    async (id) => {
                        await this.setState({ itemType: id, itemColor: '' });
                        await this.getColor();
                    },
                    this.state.arrType.length > 0 ? false : true
                )}
                {this.renderItem(
                    this.state.itemColor ? _.find(this.state.arrColor, { id: this.state.itemColor }).name : 'Màu',
                    this.state.arrColor,
                    (id) => {
                        this.setState({ itemColor: id })
                    },
                    this.state.arrColor.length > 0 ? false : true
                )}


                {this.renderItemTextInput('Năm', this.state.year, (val) => { this.setState({ year: val }) }, 4)}
                {this.renderItemTextInput('Km hiện tại', this.state.km, (val) => { this.setState({ km: val }) }, 6)}
                {this.renderItemTextInput('Biển số', this.state.license, (val) => { this.setState({ license: val }) }, 6)}
            </View>
        );
    }
    renderHeader() {
        return (
            <View style={{ height: 50, width: width, flexDirection: 'row', borderBottomColor: Configs.COLOR_PRIMARY, borderBottomWidth: 1 }} activeOpacity={.8}>
                <TouchableOpacity style={{ flex: 1, padding: 10 }}>
                    <Icon name='envelope-o' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 18, color: Configs.COLOR_ITEM }}>Bảo Trì Xe Máy</Text>
                </View>

                <TouchableOpacity style={{ flex: 1, padding: 10, alignItems: 'flex-end' }} activeOpacity={.8}>
                    <Icon name='cog' size={35} color={Configs.COLOR_ITEM} />
                </TouchableOpacity>

            </View>
        );
    }
    renderItem(txt, arr, action, disable) {
        return (
            <TouchableOpacity style={{
                width: width * .8,
                borderWidth: 1,
                borderColor: Configs.COLOR_ITEM,
                borderRadius: 3, padding: 5, alignItems: 'center', marginBottom: 20
            }}
                disabled={disable}
                onPress={() => Actions.itemSelect({ data: arr, title: txt, action })}>
                <Text>{txt}</Text>
            </TouchableOpacity>
        );
    }
    renderItemTextInput(txt, value, action, maxlength) {
        return (
            <View style={{
                width: width * .8,
                borderWidth: 1,
                borderColor: Configs.COLOR_ITEM,
                borderRadius: 3, padding: 5, alignItems: 'center', marginBottom: 20
            }}>
                <TextInput
                    style={{
                        width: width * .8,
                        padding: -7,
                        textAlign: 'center'
                    }}
                    maxLength={maxlength}
                    placeholder={txt}
                    value={value}
                    onChangeText={action}
                    underlineColorAndroid={'transparent'}
                />
            </View>



        );
    }

    renderBTNAdd() {
        return (
            <TouchableOpacity
                onPress={() => { this.addVehicle() }}
                style={{ position: 'absolute', bottom: 0, left: 0, right: 0, width: width, height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: this.state.disableBTN ? Configs.COLOR_BTN : Configs.COLOR_BTNDISABLE }}>
                {!this.state.disableBTN ?
                    <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>Thêm Xe</Text>
                    :
                    <ActivityIndicator color={"red"} style={{}} size={'small'} />
                }
            </TouchableOpacity>
        );
    }
}
