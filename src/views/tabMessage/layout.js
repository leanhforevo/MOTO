'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';
import { HeaderMenu } from '../../view_components/header';

const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container, { backgroundColor: '#fff' }]}>
                <StatusBarHeight />
                <HeaderMenu title={'Message Center'} />
                {this.renderItem()}
                {this.renderItem()}
                {this.renderItem()}
                {this.renderItem()}
                {this.renderItem()}
            </View>
        );
    }
    renderItem() {
        return (
            <View style={{ padding: 10, flexDirection: 'row', backgroundColor: '#e0e0e0', marginTop: 20 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Icon name={'envelope-o'} color={Configs.COLOR_PRIMARY} size={35} />
                </View>
                <View style={{ flex: 3 }}>
                    <View style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
                        <TextView style={{ fontSize: 18, fontWeight: 'bold' }}>Bảo hiểm xe máy</TextView>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'row', padding: 20 }}>
                        <View style={{ flex: 3, justifyContent: 'center' }}>
                            <Text numberOfLines={1} style={{ fontSize: 16 }}>Nội dung trích ngắn11111111111</Text>
                        </View>
                        <View style={{ marginRight: 5 }}>
                            <TextView>2/2/2222</TextView>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

}
