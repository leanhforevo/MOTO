'use strict';

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, ScrollView, Animated } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import Icon from 'react-native-vector-icons/FontAwesome';
import { PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator } from '../../view_components/rn-viewpager';
import { TextW, TextView, TextViewBold, TextViewPrimary, TextViewPrimaryBold } from '../../view_components/text';

const { width, height } = Dimensions.get('window');
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff', padding: 5 }}>
                <View style={{ backgroundColor: Configs.COLOR_ITEM, borderRadius: 4, padding: 10, width: width / 2, justifyContent: 'center', alignItems: 'center' }}>
                    <TextW>2017</TextW>
                </View>
                <View style={{ borderWidth: 1, borderColor: Configs.COLOR_PRIMARY, borderRadius: 4, marginTop: 10, padding: 10 }}>
                    {this.renderItemHistory()}
                    {this.renderItemHistory()}
                    {this.renderItemHistory()}
                </View>
            </View>
        );
    }
    renderItemHistory() {
        return (
            <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1.5, justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{
                        backgroundColor: Configs.COLOR_PRIMARY,
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: 10,
                        borderRadius: 4,
                        elevation: 5
                    }}>
                        <TextW>Lần 1</TextW>
                    </View>
                </View>
                <View style={{
                    flex: 3.5,
                    backgroundColor: '#fff',
                    justifyContent: 'center',
                    padding: 10,
                    borderWidth: 1,
                    borderColor: Configs.COLOR_PRIMARY,
                    borderRadius: 4
                }}>
                    <TextView>Thay: lốp xe</TextView>
                    <TextView>Bảo trì: Chưa hoàn thành</TextView>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity style={{
                        backgroundColor: Configs.COLOR_ITEM,
                        justifyContent: 'center',
                        alignItems: 'center',
                        padding: 10,
                        borderRadius: 4, marginLeft: 2
                    }} activeOpacity={.8} onPress={() => { alert('details') }}>
                        <TextW>Chi Tiết</TextW>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}
