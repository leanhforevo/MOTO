'use strict';

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Scene, Modal, Schema, Actions, ActionConst } from 'react-native-router-flux';

import Launcher from './launcher';
import Home from './home';
import Login from './account_login';
import Register from './account_register';
import MainCar from './listBike';
import MainCarAdd from './mainCarAdd';
import ItemSelect from './mainCarAdd/itemSelectec.js';

// import CarDetailInfor from './carDetailInfor';
import CarDetailInforChart from './carDetailInforChart';
import MaintenanceHistory from './maintenanceHistory';
import MaintenanceHistoryDetail from './maintenanceHistory.detail';
import CarDetailGas from './carDetailGas';
import CarDetailClean from './carDetailClean';
import Maintenance from './maintenance';
import MaintenanceDetail from './maintenance.detail';
import MaintenanceBook from './maintenanceBook';
import MaintenanceList from './maintenanceList';
import FeedBack from './feedback';
import TabIcon from '../view_components/tabIcon';
import PopupInput from '../view_components/popupInput';
import Drawer from '../view_components/drawer';
import Calendar from '../view_components/calendar';
// import HeaderMessage from './headerMessage';
import HeaderSetting from './headerSetting';
import AccountInfo from './accountInfor';

import TabMessage from './tabMessage';
import TabShare from './tabShare';
import TabGif from './tabGif';
import TabInfor from './tabInfor';

const views = Actions.create(
    <Scene key="modal" component={Modal}>


        <Scene key="root" duration={400} hideNavBar>
            <Scene key="launcher" component={Launcher} hideNavBar initial />
            <Scene key="home" component={Home} hideNavBar panHandlers={null} />
            <Scene key="login" component={Login} hideNavBar />
            <Scene key="register" component={Register} hideNavBar />
            <Scene key="mainCar" component={MainCar} hideNavBar panHandlers={null} />
            <Scene key="mainCarAdd" component={MainCarAdd} hideNavBar panHandlers={null} />
            <Scene key="drawer" type={ActionConst.RESET} component={Drawer} open={false} >
                <Scene key="sidemenu">
                    <Scene key="setting" component={HeaderSetting} hideNavBar panHandlers={null} />
                    <Scene key="accountInfo" component={AccountInfo} hideNavBar panHandlers={null} />
                    <Scene key="maintenanceHistory" component={MaintenanceHistory} ActionConst hideNavBar panHandlers={null} />
                    <Scene key="maintenanceHistoryDetail" component={MaintenanceHistoryDetail} hideNavBar panHandlers={null} />
                    <Scene key="carDetailGas" component={CarDetailGas} hideNavBar panHandlers={null} />
                    <Scene key="carDetailClean" component={CarDetailClean} hideNavBar panHandlers={null} />
                    <Scene key="maintenance" component={Maintenance} hideNavBar panHandlers={null} />
                    <Scene key="maintenanceDetail" component={MaintenanceDetail} hideNavBar panHandlers={null} />
                    <Scene key="maintenanceBook" component={MaintenanceBook} hideNavBar panHandlers={null} />
                    <Scene key="maintenanceList" component={MaintenanceList} hideNavBar panHandlers={null} />
                    <Scene key="carDetailInforChart" component={CarDetailInforChart} hideNavBar panHandlers={null} />
                    <Scene key="feedBack" component={FeedBack} hideNavBar panHandlers={null} />
                    <Scene key="main" tabs={true} duration={500} panHandlers={null} initial
                        tabBarStyle={{
                            borderTopWidth: 1,
                            borderTopColor: '#CCCCCC',
                            backgroundColor: '#fff'
                        }}>
                        <Scene icon={TabIcon} key="tabInfor" component={TabInfor} hideNavBar initial />
                        <Scene icon={TabIcon} key="tabMessage" component={TabMessage} hideNavBar />
                        {/* <Scene icon={TabIcon} key="tabGif" component={TabGif} hideNavBar />
                        <Scene icon={TabIcon} key="tabShare" component={TabShare} hideNavBar /> */}
                    </Scene>
                </Scene>
            </Scene>
        </Scene>
        <Scene key="popUpInput" component={PopupInput} hideNavBar />
        <Scene key="itemSelect" component={ItemSelect} hideNavBar />
        <Scene key="calendar" component={Calendar} hideNavBar />
    </Scene>
);

export default views;
