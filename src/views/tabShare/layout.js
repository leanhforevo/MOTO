'use strict';

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Configs from '../../configs';
import style from './style';
import StatusBarHeight from '../../view_components/statusbarHeight';
import { HeaderMenu } from '../../view_components/header';
export default class Layout extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={[style.container]}>
                <StatusBarHeight />
                <HeaderMenu title={'Chia sẻ'} />
            </View>
        );
    }
}
