'use strict';

const styles = {
    buttonColors: [
        'rgba(255, 89, 12, 1)',
        'rgba(17, 28, 32, 1)',
        'rgba(51, 51, 61, 1)',
        'rgba(128, 128, 128, 1)',
        'rgba(179, 178, 179, 1)',
        'rgba(214, 214, 214, 1)',
        'rgba(247, 247, 247, 1)',
        'rgba(249, 249, 249, 1)',
        'rgba(255, 255, 255, 1)'
    ]
};

export default styles;
