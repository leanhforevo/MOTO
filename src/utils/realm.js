'use strict';

import Realm from 'realm';

export default class RealmDB {

    constructor() {
        this.realm = null;
    }
    /**
     * schemas : Array
     */
    connect(schema) {
        let schemas = Object.keys(schema).reduce((total, value) => total.concat(schema[value]), []);
        this.realm = new Realm({ schema: schemas });
    }

    get connection() {
        return this.realm;
    }

    write(callback) {
        return this.realm.write(() => callback(this.realm));
    }

    objects(schemaName) {
        return this.realm.objects(schemaName);
    }
}