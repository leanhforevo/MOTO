
'use strict';

import { Dimensions } from 'react-native';
import Configs from '../configs';

const { width, height } = Dimensions.get('window');


export default class {

    //  1. Scale normally

    //      a. No filter
    static fetchDataAPI(url, callback) {
        try {
            fetch(url)
                .then(response => { console.log(response); return response.json() })
                .then(val => callback(val))
        } catch (error) {
            callback(false)
        }
    }
}


