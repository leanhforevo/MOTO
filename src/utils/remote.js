'use strict';
import Configs from '../configs';

export default class Remote {

    static request(url, method, body) {
        let request = {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                // 'c' : 'd'
            },
        };
        if (body.token) {
            request.headers['Authorization'] = "Bearer " + body.token;
        }

        if (method == 'POST') {
            request["method"] = method;
            request["body"] = JSON.stringify(body);
        }

        return fetch(url, request).then(response => response.json());
    }

    static json(path) {
        var api = path;
        var request = {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        };
        return fetch(api, request)
            .then(response => response.json())
            .catch(error => console.log(error));
    }

}
