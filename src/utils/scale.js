
'use strict';

import { Dimensions } from 'react-native';
import Configs from '../configs';

const { width, height } = Dimensions.get('window');

export default class {

    //  1. Scale normally

    //      a. No filter
    static scaleWidth_Standard(size) {
        return width * size / Configs.DEFAULT_WIDTH;
    }

    static scaleHeight_Standard(size) {
        return height * size / Configs.DEFAULT_HEIGHT;
    }

    //      b. To Int
    static scaleWidthToInt_Standard(size) {
        return parseInt(width * size / Configs.DEFAULT_WIDTH);
    }

    static scaleHeightToInt_Standard(size) {
        return parseInt(height * size / Configs.DEFAULT_HEIGHT);
    }

    //      c. Accurate to 1/10
    static scaleWidthDecOne_Standard(size) {
        return parseInt(width * size * 10 / Configs.DEFAULT_WIDTH) / 10;
    }

    static scaleHeightDecOne_Standard(size) {
        return parseInt(height * size * 10 / Configs.DEFAULT_HEIGHT) / 10;
    }


    // 2. Bigger screen included
    //      a. No filter
    static scaleWidth_Optional(size) {
        return width < Configs.DEFAULT_WIDTH ? width * size / Configs.DEFAULT_WIDTH : size;
    }

    static scaleHeight_Optional(size) {
        return height < Configs.DEFAULT_HEIGHT ? height * size / Configs.DEFAULT_HEIGHT : size;
    }

    //      b. To Int
    static scaleWidthToInt_Optional(size) {
        return width < Configs.DEFAULT_WIDTH ? parseInt(width * size / Configs.DEFAULT_WIDTH) : parseInt(size);
    }

    static scaleHeightToInt_Optional(size) {
        return height < Configs.DEFAULT_HEIGHT ? parseInt(height * size / Configs.DEFAULT_HEIGHT) : parseInt(size);
    }

    //      c. Accurate to 1/10
    static scaleWidthDecOne_Optional(size) {
        return width < Configs.DEFAULT_WIDTH ? parseInt(width * size * 10 / Configs.DEFAULT_WIDTH) / 10 : size;
    }

    static scaleHeightDecOne_Optional(size) {
        return height < Configs.DEFAULT_HEIGHT ? parseInt(height * size * 10 / Configs.DEFAULT_HEIGHT) / 10 : size;
    }
}
