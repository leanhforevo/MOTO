'use strict';

import { AppRegistry } from 'react-native';
import { Component } from 'react';
import Source from './src';

AppRegistry.registerComponent('MOTO', () => Source);
